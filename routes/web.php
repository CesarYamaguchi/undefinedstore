<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|


Route::get('/', function () {
    return view('welcome');
});
*/

//Route::get('/', 'principalController@LogIn');

//Route::post('/validarLogIn', 'principalController@validarLogIn');

Route::get('/inicio', 'principalController@index');

Route::get('/registrarArticulo', 'principalController@registrarArticulo');

Route::get('/registrarUsuario', 'principalController@registrarUsuario');

Route::get('/registrarCategoria', 'principalController@registrarCategoria');

Route::get('/registrarSubcategoria', 'principalController@registrarSubcategoria');

//Articulos
Route::post('/guardarArticulo', 'articulosController@guardar');

Route::get('/consultarArticulos', 'articulosController@consultar');

Route::get('/actualizarArticulo/{id}', 'articulosController@actualizarArticulo');

Route::post('/actualizaArticulo/{id}', 'articulosController@actualizar');

Route::get('/configurarArticulo/{id}', 'articulosController@configurarArticulo');

Route::get('/catalogo/{id_categoria}/{id_subcategoria}', 'articulosController@catalogo');

Route::get('/descripcionArticulo/{id_articulo}/{id_usuario}', 'articulosController@descripcionArticulo');

Route::get('/pdfArticulos', 'articulosController@pdfArticulos');

Route::post('/calificarArticulo/{id_usuario}/{id_articulo}','articulosController@calificarArticulo');


//Articulo_imagenes
Route::post('/guardarArticuloImagen/{id}', 'articulo_imagenesController@guardar');

Route::get('/quitarArtImg/{id_art}/{id_img}', 'articulo_imagenesController@quitarImg');

Route::get('/principalArtImg/{id_art}/{id_img}', 'articulo_imagenesController@principalImg');

//Usuarios
Route::post('/guardarUsuario', 'usuariosController@guardar');

Route::get('/consultarUsuarios', 'usuariosController@consultar');

Route::get('/actualizarUsuario/{id}', 'usuariosController@actualizarUsuario');

Route::post('/actualizaUsuario/{id}', 'usuariosController@actualizar');

Route::get('/perfil', 'usuariosController@perfil');

Route::get('/pdfUsuarios', 'usuariosController@pdfUsuarios');

//Categorias
Route::post('/guardarCategoria', 'categoriasController@guardar');

Route::get('/consultarCategorias', 'categoriasController@consultar');

Route::get('/actualizarCategoria/{id}', 'categoriasController@actualizarCategoria');

Route::post('/actualizaCategoria/{id}', 'categoriasController@actualizar');

Route::get('/configurarCategoria/{id}', 'categoriasController@configurarCategoria');

Route::post('/agregarSubcategoria/{id}', 'categoriasController@agregarSubcategoria');

Route::get('/quitarSubcategoria/{id_categoria}/{id_subcategoria}', 'categoriasController@quitarSubcategoria');

Route::get('/pdfCategorias', 'categoriasController@pdfCategorias');

//Subcategorias
Route::post('/guardarSubcategoria', 'subcategoriasController@guardar');

Route::get('/consultarSubcategorias', 'subcategoriasController@consultar');

Route::get('/actualizarSubcategoria/{id}', 'subcategoriasController@actualizarSubcategoria');

Route::post('/actualizaSubcategoria/{id}', 'subcategoriasController@actualizar');

Route::get('/configurarSubcategoria/{id}', 'subcategoriasController@configurarSubcategoria');

Route::post('/agregarArticulo/{id}', 'subcategoriasController@agregarArticulo');

Route::get('/quitarArticulo/{id_subcategoria}/{id_Articulo}', 'subcategoriasController@quitarArticulo');

//Comentarios

Route::post('/guardarComentario/{id}', 'comentariosController@guardarComentario');

Route::get('/reportarComentario/{id}','comentariosController@reportarComentario');

Route::post('/reportar','comentariosController@reportar');

Route::get('/leerReporte/{id_reporte}','comentariosController@leerReporte');

Route::get('/eliminarComentario/{id}','comentariosController@eliminarComentario');

Auth::routes();

Route::get('/', 'HomeController@index');

//Carrito

//Método genérico para obtener la información de un artículo
/*Route::bind('articulo',function($id){
	return App\articulos::where('id',$id)->first();
});*/

Route::get('/carrito/mostrar/{id_usuario}','carritoController@mostrar');

Route::get('/carrito/agregar/{id_articulo}/{id_usuario}','carritoController@agregar');

Route::get('/carrito/eliminar/{id_usuario}/{id_articulo}','carritoController@eliminar');

Route::get('/carrito/vaciar/{id_usuario}','carritoController@vaciar');

Route::get('/carrito/actualizar/{id_usuario}/{id_articulo}/{cantidad}','carritoController@actualizar');

Route::get('/carrito/pedido/{id_usuario}','pedidosController@generar');

Route::get('/carrito/pedido/cancelar/{id_pedido}','pedidosController@cancelar');

Route::get('/correo/enviar/','correoController@enviar');