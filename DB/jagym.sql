-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-12-2016 a las 02:44:41
-- Versión del servidor: 10.1.8-MariaDB
-- Versión de PHP: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `jagym`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `precio_compra` decimal(19,4) NOT NULL,
  `precio_venta` decimal(19,4) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `status` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id`, `nombre`, `descripcion`, `precio_compra`, `precio_venta`, `cantidad`, `status`, `created_at`, `updated_at`) VALUES
(1, 'procesador3', 'descripcion', '10.0000', '15.0000', 5, b'1', '2016-11-07 04:06:08', '2016-11-07 11:06:08'),
(2, 'Prueba2', 'hello', '1.0000', '2.0000', 6, b'0', '2016-11-03 18:45:26', '2016-11-04 01:45:26'),
(3, 'ram', 'asdf', '234.0000', '236.0000', 3, b'1', '2016-11-06 19:15:23', '2016-11-07 02:15:23'),
(4, 'procesador', 'sdf', '144.0000', '555.0000', 4, b'1', '2016-11-07 02:14:42', '2016-11-07 02:14:42'),
(5, 'procesador2', 'asdf', '12.0000', '13.0000', 3, b'1', '2016-11-07 11:05:17', '2016-11-07 11:05:17'),
(6, 'cpu5', 'mejor', '500.0000', '600.0000', 5, b'1', '2016-11-16 11:12:04', '2016-11-16 11:12:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos_pedidos`
--

CREATE TABLE `articulos_pedidos` (
  `id_pedido` int(11) NOT NULL,
  `id_articulo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `status` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos_subcategorias`
--

CREATE TABLE `articulos_subcategorias` (
  `id_articulo` int(11) NOT NULL,
  `id_subcategoria` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `articulos_subcategorias`
--

INSERT INTO `articulos_subcategorias` (`id_articulo`, `id_subcategoria`, `created_at`, `updated_at`) VALUES
(1, 2, '2016-11-07 11:06:42', '2016-11-07 11:06:42'),
(3, 6, '2016-11-07 11:09:19', '2016-11-07 11:09:19'),
(4, 1, '2016-11-07 10:37:07', '2016-11-07 10:37:07'),
(5, 1, '2016-11-07 11:37:46', '2016-11-07 11:37:46'),
(6, 2, '2016-11-16 11:12:34', '2016-11-16 11:12:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo_imagenes`
--

CREATE TABLE `articulo_imagenes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `principal` bit(1) NOT NULL,
  `id_articulo` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `articulo_imagenes`
--

INSERT INTO `articulo_imagenes` (`id`, `nombre`, `principal`, `id_articulo`, `created_at`, `updated_at`) VALUES
(2, 'cpu2.jpg', b'0', 1, '2016-11-07 15:15:24', '2016-11-07 00:44:56'),
(3, 'cpu3.jpg', b'0', 1, '2016-11-07 04:06:59', '2016-11-07 00:45:06'),
(4, 'cpu4.jpg', b'0', 1, '2016-11-06 18:53:01', '2016-11-07 00:45:10'),
(5, 'cpu5.jpg', b'0', 1, '2016-11-07 00:45:14', '2016-11-07 00:45:14'),
(8, 'cpu2.jpg', b'0', 2, '2016-11-07 02:08:50', '2016-11-07 02:08:50'),
(9, 'cpu4.jpg', b'1', 2, '2016-11-06 19:08:56', '2016-11-07 02:08:55'),
(10, 'ram4.jpg', b'1', 3, '2016-11-06 19:09:17', '2016-11-07 02:09:15'),
(12, 'cpu5.jpg', b'1', 4, '2016-11-06 20:01:56', '2016-11-07 03:01:54'),
(13, 'cpu1.png', b'1', 5, '2016-11-07 04:05:43', '2016-11-07 11:05:41'),
(14, 'cpu7.jpg', b'1', 1, '2016-11-07 15:15:24', '2016-11-07 11:06:56'),
(15, 'cpu8.jpg', b'1', 6, '2016-11-16 04:12:15', '2016-11-16 11:12:12');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calificaciones`
--

CREATE TABLE `calificaciones` (
  `id` int(11) NOT NULL,
  `calificacion` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_articulo` int(11) NOT NULL,
  `status` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `id_usuario` int(11) NOT NULL,
  `id_articulo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `carrito`
--

INSERT INTO `carrito` (`id_usuario`, `id_articulo`, `cantidad`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2016-11-16 12:24:35', '2016-11-16 12:24:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Procesadores', b'1', '2016-11-04 16:42:30', '2016-11-04 23:42:30'),
(2, 'RAM', b'1', '2016-11-06 20:37:04', '2016-11-07 03:37:04'),
(3, 'GPU', b'0', '2016-11-06 20:37:14', '2016-11-07 03:37:14'),
(4, 'Disco duro', b'0', '2016-11-06 20:37:28', '2016-11-07 03:37:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias_subcategorias`
--

CREATE TABLE `categorias_subcategorias` (
  `id_categoria` int(11) NOT NULL,
  `id_subcategoria` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `categorias_subcategorias`
--

INSERT INTO `categorias_subcategorias` (`id_categoria`, `id_subcategoria`, `created_at`, `updated_at`) VALUES
(1, 1, '2016-11-06 20:41:36', '0000-00-00 00:00:00'),
(1, 2, '2016-11-06 20:41:52', '0000-00-00 00:00:00'),
(2, 6, '2016-11-07 07:47:15', '2016-11-07 07:47:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL,
  `comentario` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `id_articulo` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `status` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`id`, `comentario`, `id_articulo`, `id_usuario`, `status`, `created_at`, `updated_at`) VALUES
(1, 'muy bien', 3, 1, b'0', '2016-11-16 04:35:32', '2016-11-16 04:35:32'),
(2, 'hola', 3, 1, b'0', '2016-11-16 04:35:40', '2016-11-16 04:35:40'),
(3, 'hola', 1, 1, b'0', '2016-11-16 11:32:17', '2016-11-16 11:32:17'),
(4, 'hello', 1, 1, b'0', '2016-11-16 11:32:28', '2016-11-16 11:32:28'),
(5, 'comentario de ejemplo\r\n', 1, 1, b'1', '2016-11-16 11:40:51', '2016-11-16 11:40:51'),
(6, 'comentario antonio', 1, 2, b'1', '2016-11-16 11:44:00', '2016-11-16 11:44:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metodos_envio`
--

CREATE TABLE `metodos_envio` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metodos_pago`
--

CREATE TABLE `metodos_pago` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `status` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recepciones`
--

CREATE TABLE `recepciones` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `status` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reportes_comentarios`
--

CREATE TABLE `reportes_comentarios` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_comentario` int(11) NOT NULL,
  `descripcion` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `leido` bit(1) NOT NULL,
  `status` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategorias`
--

CREATE TABLE `subcategorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `subcategorias`
--

INSERT INTO `subcategorias` (`id`, `nombre`, `status`, `created_at`, `updated_at`) VALUES
(1, 'intel', b'1', '2016-11-04 23:47:04', '2016-11-04 23:47:04'),
(2, 'AMD', b'1', '2016-11-04 16:47:23', '2016-11-04 23:47:23'),
(3, 'hp', b'1', '2016-11-06 20:37:44', '2016-11-07 03:37:44'),
(4, 'compaq', b'1', '2016-11-06 21:00:04', '2016-11-07 04:00:04'),
(5, 'negro', b'0', '2016-11-06 20:38:16', '2016-11-07 03:38:16'),
(6, 'kingston', b'1', '2016-11-07 04:00:56', '2016-11-07 04:00:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sexo` bit(1) DEFAULT NULL,
  `telefono` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin` bit(1) NOT NULL DEFAULT b'0',
  `imagen` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'defaultuser.png',
  `status` bit(1) NOT NULL DEFAULT b'1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `sexo`, `telefono`, `direccion`, `admin`, `imagen`, `status`, `created_at`, `updated_at`) VALUES
(1, 'cesar', 'cesar_yamaguchi12@hotmail.com', '$2y$10$mSak5efpEUPe9zRIcLg0WeK3ajn4rZPKI/pccqQXtEgl0jtgwHEby', 'PAqqvQNdpXAUMXn90vCuvtusa1eH2BJG7fgM0vfCNF5K2eaSRmBml9kGKeSE', b'0', '', '', b'1', 'us2.jpg', b'1', '2016-11-16 07:16:46', '2016-11-16 23:00:18'),
(2, 'antonio', 'antonio@hotmail.com', '$2y$10$RuqjOyFxsSbJbmUM7o6yhev6edgMqrvR1vJHbU1ruJ.yP62m8NoS.', 'oIhSQfC2NIs6nKWD0lno2DIqmQj7lxTCZlh5Yx0npfNnA5KPMugqrOwe9UvS', b'0', '', '', b'0', 'us4.jpg', b'1', '2016-11-16 07:20:31', '2016-11-16 23:00:53'),
(3, 'misael', 'misael@hotmail.com', '$2y$10$ydnQoR5gu25nK6xeAIdU2u9d3bw2s9QdqmIpSAsRpK4Bnr.oKrXuS', 'uiT6jrYyfTM2UhjcKnXO1cMc6P6kzMrinpJBeUOcsZbZtUbNgSEoUc4DZWCn', b'0', '', '', b'0', 'us8.jpg', b'1', '2016-11-16 09:41:31', '2016-11-16 11:22:38'),
(4, 'german', 'german@hotmail.com', '$2y$10$ayRVmpfGF6KNhRsRIX1ZveJ8QatUaWcPuChNs6hFhf15oHaXGisJm', 'DKawUg4uYIB8DHjJPaWnuRlAtD0ulYeEojSrzCC6yS1EwMQ5tgF35gMhwypQ', b'0', '', '', b'0', 'us7.jpg', b'1', '2016-11-16 09:44:06', '2016-11-16 11:22:51'),
(5, 'jonathan', 'jonathan@hotmail.com', '$2y$10$5fvy/ojZFMX8W3cYJ4GZU.6bpyxFuxihbQYJO58L5wI2tGum/w83S', 'T0yBDmUBPKSXWRcLDrDFIjDr2PspoIGNe650wLdKthIrbdT7BKozFQUOIx9o', b'0', '', '', b'0', 'us9.jpg', b'1', '2016-11-16 09:45:28', '2016-11-16 11:22:59'),
(6, 'jose', 'jose@hotmail.com', '123456', NULL, b'0', '', '', b'0', 'defaultuser.png', b'1', '2016-11-16 10:13:17', '2016-11-16 10:13:17'),
(7, 'fran', 'fran@gmail.com', '123456', NULL, b'0', '', '', b'0', 'us4.jpg', b'1', '2016-11-16 10:32:06', '2016-11-16 10:32:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `admin` bit(1) NOT NULL,
  `status` bit(1) NOT NULL,
  `sexo` bit(1) NOT NULL,
  `imagen` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellidos`, `telefono`, `direccion`, `email`, `usuario`, `password`, `admin`, `status`, `sexo`, `imagen`, `created_at`, `updated_at`) VALUES
(1, 'usuario1', 'ape1', '541', 'sdf', 'ASDF', '', '', b'1', b'1', b'1', '', '2016-11-04 02:31:35', '2016-11-04 03:42:08'),
(2, 'usuario2', 'as', '234', '234', '234awer', '', '', b'0', b'1', b'0', '', '2016-11-04 02:45:59', '2016-11-04 02:45:59'),
(3, 'admin', 'admin', '23452', 'dfs', 'sadfa@asdf.sdf', 'Admin', 'admin', b'1', b'0', b'0', 'us1.jpg', '2016-11-04 05:00:45', '2016-11-06 22:33:07'),
(5, 'antonio', 'garcia', '2135165', 'SDFA', 'g@sa.asd', 'antonio13', '123456', b'1', b'1', b'0', 'us2.jpg', '2016-11-05 02:18:52', '2016-11-05 02:18:52'),
(6, 'antonio', 'garcia', '2135165', 'SDFA', 'g@sa.asd', 'antonio14', '123456', b'1', b'1', b'0', 'us2.jpg', '2016-11-05 02:19:49', '2016-11-06 22:32:36'),
(7, 'jesus', 'asdgf', '35165', 'asdf', 'g@sa.asd', 'jesus', '123456', b'1', b'1', b'0', 'img.jpg', '2016-11-05 02:22:58', '2016-11-05 02:22:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` int(11) NOT NULL,
  `importe` decimal(19,4) NOT NULL,
  `id_metodo_pago` int(11) NOT NULL,
  `id_metodo_envio` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `status` bit(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas_articulos`
--

CREATE TABLE `ventas_articulos` (
  `id_venta` int(11) NOT NULL,
  `id_articulo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(19,4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `articulos_pedidos`
--
ALTER TABLE `articulos_pedidos`
  ADD PRIMARY KEY (`id_pedido`,`id_articulo`),
  ADD KEY `id_pedido` (`id_pedido`,`id_articulo`),
  ADD KEY `id_articulo` (`id_articulo`);

--
-- Indices de la tabla `articulos_subcategorias`
--
ALTER TABLE `articulos_subcategorias`
  ADD PRIMARY KEY (`id_articulo`,`id_subcategoria`),
  ADD KEY `articuloFK` (`id_articulo`),
  ADD KEY `subcategoriaFK` (`id_subcategoria`);

--
-- Indices de la tabla `articulo_imagenes`
--
ALTER TABLE `articulo_imagenes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articuloFK` (`id_articulo`);

--
-- Indices de la tabla `calificaciones`
--
ALTER TABLE `calificaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD PRIMARY KEY (`id_usuario`,`id_articulo`),
  ADD KEY `id_articulo` (`id_articulo`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias_subcategorias`
--
ALTER TABLE `categorias_subcategorias`
  ADD PRIMARY KEY (`id_categoria`,`id_subcategoria`),
  ADD KEY `categoriaFK` (`id_categoria`),
  ADD KEY `subcategoriaFK` (`id_subcategoria`);

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_articulo` (`id_articulo`,`id_usuario`),
  ADD KEY `id_usuario` (`id_usuario`);

--
-- Indices de la tabla `metodos_pago`
--
ALTER TABLE `metodos_pago`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuarioFK` (`id_usuario`);

--
-- Indices de la tabla `recepciones`
--
ALTER TABLE `recepciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reportes_comentarios`
--
ALTER TABLE `reportes_comentarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usuarioFK` (`id_usuario`),
  ADD KEY `comentarioFK` (`id_comentario`);

--
-- Indices de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ventas_articulos`
--
ALTER TABLE `ventas_articulos`
  ADD PRIMARY KEY (`id_venta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `articulo_imagenes`
--
ALTER TABLE `articulo_imagenes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `calificaciones`
--
ALTER TABLE `calificaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `metodos_pago`
--
ALTER TABLE `metodos_pago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `recepciones`
--
ALTER TABLE `recepciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `reportes_comentarios`
--
ALTER TABLE `reportes_comentarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ventas_articulos`
--
ALTER TABLE `ventas_articulos`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulos_pedidos`
--
ALTER TABLE `articulos_pedidos`
  ADD CONSTRAINT `articulos_pedidos_ibfk_1` FOREIGN KEY (`id_pedido`) REFERENCES `pedidos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `articulos_pedidos_ibfk_2` FOREIGN KEY (`id_articulo`) REFERENCES `articulos` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `articulos_subcategorias`
--
ALTER TABLE `articulos_subcategorias`
  ADD CONSTRAINT `articulos_subcategorias_ibfk_1` FOREIGN KEY (`id_articulo`) REFERENCES `articulos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `articulos_subcategorias_ibfk_2` FOREIGN KEY (`id_subcategoria`) REFERENCES `subcategorias` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `articulo_imagenes`
--
ALTER TABLE `articulo_imagenes`
  ADD CONSTRAINT `articulo_imagenes_ibfk_1` FOREIGN KEY (`id_articulo`) REFERENCES `articulos` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD CONSTRAINT `carrito_ibfk_2` FOREIGN KEY (`id_articulo`) REFERENCES `articulos` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `carrito_ibfk_3` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `categorias_subcategorias`
--
ALTER TABLE `categorias_subcategorias`
  ADD CONSTRAINT `categorias_subcategorias_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `categorias_subcategorias_ibfk_2` FOREIGN KEY (`id_subcategoria`) REFERENCES `subcategorias` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `comentarios_ibfk_1` FOREIGN KEY (`id_articulo`) REFERENCES `articulos` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD CONSTRAINT `pedidos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `reportes_comentarios`
--
ALTER TABLE `reportes_comentarios`
  ADD CONSTRAINT `reportes_comentarios_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `reportes_comentarios_ibfk_2` FOREIGN KEY (`id_comentario`) REFERENCES `comentarios` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
