<!DOCTYPE html>
<html lang="en" style="margin: 0;">
<head>
	<meta charset="utf-8">
	<title>Listado completo de Articulos</title>
	<link rel="stylesheet" type="text/css" href="{{asset("css/styles.css")}}"> 
</head>
<body>
	<div class="box" align="center">
		<h1>Lista de articulos</h1>
	</div>
	
	<div class="box">
		<table class="tableMod" align="center">
			<thead>
				<tr>
					<th>#</th>
					<th>Nombre</th>
					<th>Descripcion</th>
					<th>Precio compra</th>
					<th>Precio venta</th>
					<th>cantidad</th>
					<th>status</th>
				</tr>
			</thead>
			@foreach($articulos as $a)					
				<tr>
					<td>{{$a->id}}</td>
					<td>{{$a->nombre}}</td>
					<td>{{$a->descripcion}}</td>
					<td>{{$a->precio_compra}}</td>
					<td>{{$a->precio_venta}}</td>
					<td>{{$a->cantidad}}</td>
					<td>{{$a->status}}</td>
				</tr>
			@endforeach
		</table>	
	</div>
	
	<footer><p align="center">Copyright &copy; JAGYM H.S. Ago-Dic 2016</p></footer>
</body>
</html>