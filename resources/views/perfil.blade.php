@extends('principal')
@section('encabezado')
	<h1>Perfil</h1>
@stop

@section('contenido')
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
          	<div class="x_panel">
	            <div class="x_title">
		            <h2>Actualizar datos del perfil</h2>
	              	<div class="clearfix"></div>
	            </div>
	            <div class="x_content">
	            	<form method="POST" action="{{url('/actualizaUsuario')}}/{{ Auth::user()->id }}" class="form-horizontal form-label-left" accept-charset="UTF-8" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
		          		<div class="item form-group">
		            		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">Nombre <span class="required">*</span>
		            		</label>
				            <div class="col-md-6 col-sm-6 col-xs-12">
				              <input id="nombre" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="nombre" placeholder="" required="required" type="text" value="{{Auth::user()->name}}">
				            </div>
		          		</div>
		          		<div class="item form-group">
		            		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">Telefono </label>
				            <div class="col-md-6 col-sm-6 col-xs-12">
				              <input type="tel" id="telefono" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="telefono" placeholder="" type="text" value="{{Auth::user()->telefono}}">
				            </div>
		          		</div>
		          		<div class="item form-group">
		            		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="direccion">Dirección </label>
				            <div class="col-md-6 col-sm-6 col-xs-12">
				              <input id="direccion" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="direccion" placeholder="" type="text" value="{{Auth::user()->direccion}}">
				            </div>
		          		</div>
				        <div class="item form-group">
				            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sexo">Sexo <span class="required">*</span>
				            </label>
				            <div class="col-md-6 col-sm-6 col-xs-12">
								<select name="sexo" id="sexo" class="form-control col-md-7 col-xs-12" required>
									<option value="">Sexo</option>
									@if(Auth::user()->sexo == 0)
										<option value="0" selected>Masculino</option>
										<option value="1">Femenino</option>
									@else
										<option value="0">Masculino</option>
										<option value="1" selected>Femenino</option>
									@endif
								</select>
				            </div>
				        </div>
				        <div class="item form-group">
		            		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="file">Imagen </label>
				            <div class="col-md-6 col-sm-6 col-xs-12">
		                  			<input type="file" class="form-control" name="file" >
				            </div>
		          		</div>

		          
		          		<div class="ln_solid"></div>
		          		<div class="form-group">
				            <div class="col-md-6 col-md-offset-3">
				           		<a href="{{url('/consultarUsuarios')}}" class="btn btn-danger">Cancelar</a>
				              	<input type="submit" class="btn btn-success">
				            </div>
		          		</div>
		        	</form>
	            </div>
          	</div>
        </div>
	</div>
@stop