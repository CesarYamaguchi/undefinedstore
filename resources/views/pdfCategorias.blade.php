<!DOCTYPE html>
<html lang="en" style="margin: 0;">
<head>
	<meta charset="utf-8">
	<title>Listado completo de Categorias</title>
	<link rel="stylesheet" type="text/css" href="{{asset("css/styles.css")}}"> 
</head>
<body>
	<div class="box" align="center">
		<h1>Lista de categorias</h1>
	</div>
	
	<div class="box">
		<table class="tableMod" align="center">
			<thead>
				<tr>
					<th>#</th>
					<th>Nombre</th>
					<th>status</th>
				</tr>
			</thead>
			@foreach($categorias as $c)
				<tr>
					<td class="centrado">{{$c->id}}</td>
					<td>{{$c->nombre}}</td>
					<td>{{$c->status}}</td>
				</tr>
			@endforeach
		</table>
	</div>
	
	<footer><p align="center">Copyright &copy; JAGYM H.S. Ago-Dic 2016</p></footer>
</body>
</html>