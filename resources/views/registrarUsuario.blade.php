@extends('principal')
@section('encabezado')
	<h1>Registrar Usuario</h1>
@stop

@section('contenido')
	<div class="x_panel">
	    <div class="x_title">
	        <h2>Registrar usuario</h2>
	        <div class="clearfix"></div>
	    </div>
        <div class="x_content">
			<form method="POST" action="{{url('/guardarUsuario')}}" class="form-horizontal form-label-left" accept-charset="UTF-8" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
          		<span class="section">Información personal</span>
          		<div class="item form-group">
            		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">Nombre <span class="required">*</span>
            		</label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              <input id="nombre" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="nombre" placeholder="" required="required" type="text">
		            </div>
          		</div>
          		<div class="item form-group">
            		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">email <span class="required">*</span>
            		</label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              <input type="email" id="email" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="email" placeholder="" required="required" type="text">
		            </div>
          		</div>
          		<div class="item form-group">
            		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Contraseña <span class="required">*</span>
            		</label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              <input type="password" id="password" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="password" placeholder="" required="required" type="text">
		            </div>
          		</div>
          		<div class="item form-group">
            		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="telefono">Telefono </label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              <input type="tel" id="telefono" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="telefono" placeholder="" type="text">
		            </div>
          		</div>
          		<div class="item form-group">
            		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="direccion">Dirección </label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              <input id="direccion" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="direccion" placeholder="" type="text">
		            </div>
          		</div>
		        <div class="item form-group">
		            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sexo">Sexo <span class="required">*</span>
		            </label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
						<select name="sexo" id="sexo" class="form-control col-md-7 col-xs-12" required>
							<option value="" selected>Sexo</option>
							<option value="0">Masculino</option>
							<option value="1">Femenino</option>
						</select>
		            </div>
		        </div>
		        <div class="item form-group">
            		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="file">Imagen </label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
                  			<input type="file" class="form-control" name="file" >
		            </div>
          		</div>
			
		        <div class="item form-group">
		            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="admin">Administrador</label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              <input type="checkbox" id="admin" name="admin" class="js-switch">
		            </div>
		        </div>
		        <div class="item form-group">
		            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status</label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              <input type="checkbox" id="status" name="status" class="js-switch" checked>
		            </div>
		        </div>

          		<div class="ln_solid"></div>
          		<div class="form-group">
		            <div class="col-md-6 col-md-offset-3">
		           		<a href="{{url('/inicio')}}" class="btn btn-danger">Cancelar</a>
		              	<input type="submit" class="btn btn-success">
		            </div>
          		</div>
        	</form>
      	</div>
    </div>
@stop
