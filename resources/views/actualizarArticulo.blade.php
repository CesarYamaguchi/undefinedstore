@extends('principal')
@section('encabezado')
	<h1>Actualizar Articulo</h1>
@stop

@section('contenido')
	<div class="x_panel">
	    <div class="x_title">
	        <h2>Actualizar articulo</h2>
	        <div class="clearfix"></div>
	    </div>
        <div class="x_content">
			<form method="POST" action="{{url('/actualizaArticulo')}}/{{$articulo->id}}" class="form-horizontal form-label-left">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
          		<span class="section">{{$articulo->nombre}}</span>

          		<div class="item form-group">
            		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">Nombre <span class="required">*</span>
            		</label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              	<input id="nombre" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="nombre" placeholder="" required="required" type="text" value="{{$articulo->nombre}}">
		            </div>
          		</div>
          		<div class="item form-group">
		            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descripcion">Descripción <span class="required">*</span>
		            </label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              	<textarea id="descripcion" required="required" name="descripcion" class="form-control col-md-7 col-xs-12">{{$articulo->descripcion}}</textarea>
		            </div>
		        </div>
		        <div class="item form-group">
		            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="precio_compra">Precio de compra <span class="required">*</span>
		            </label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              	<input type="number" id="precio_compra" name="precio_compra" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" value="{{$articulo->precio_compra}}">
		            </div>
		        </div>
		        <div class="item form-group">
		            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="precio_venta">Precio de venta <span class="required">*</span>
		            </label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              	<input type="number" id="precio_venta" name="precio_venta" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" value="{{$articulo->precio_venta}}">
		            </div>
		        </div>
		        <div class="item form-group">
		            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cantidad">cantidad <span class="required">*</span>
		            </label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              	<input type="number" id="cantidad" name="cantidad" required="required" data-validate-minmax="10,100" class="form-control col-md-7 col-xs-12" value="{{$articulo->cantidad}}">
		            </div>
		        </div>
		        <div class="item form-group">
		            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status <span class="required">*</span>
		            </label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		            	@if($articulo->status == 1)
		            		<input type="checkbox" id="status" name="status" class="js-switch" checked>
		            	@else
		            		<input type="checkbox" id="status" name="status" class="js-switch">
		            	@endif
		            </div>
		        </div>

          
          		<div class="ln_solid"></div>
          		<div class="form-group">
		            <div class="col-md-6 col-md-offset-3">
		           		<a href="{{url('/consultarArticulos')}}" class="btn btn-danger">Cancelar</a>
		              	<input type="submit" class="btn btn-success">
		            </div>
          		</div>
        	</form>
      	</div>
    </div>
@stop