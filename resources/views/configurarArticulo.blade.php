@extends('principal')
@section('encabezado')
	<h1>Configurar Articulo: {{$articulo->nombre}}</h1>
@stop

@section('contenido')
	<div class="row">
		<div class="x_panel">
		    <div class="x_title">
		        <h2>Imagenes</h2>
		        <div class="clearfix"></div>
		    </div>
	        <div class="x_content">
	        	<div class="x_content">
					@foreach($imagenes as $i)
						<div class="col-md-3 col-sm-4 col-xs-6">
							<div class="thumbnail" style="background-size: 100%; border-radius: 20%"  align="center">
								<img src="{{ asset('/img/articulos/'.$i->nombre) }}" width="70%" alt="{{$i->nombre}}" style="border-radius: 20%">
							</div>
						   	
						   	<div align="center">
						        <p>
						        	<a href="{{url('/quitarArtImg')}}/{{$articulo->id}}/{{$i->id}}" class="btn btn-danger btn-xs">Quitar</a>
						        	@if($i->principal == 0)
						        		<a href="{{url('/principalArtImg')}}/{{$articulo->id}}/{{$i->id}}" class="btn btn-primary btn-xs">Principal</a>
						        	@else
						        		<button class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-ok" aria-hidden="true"> Principal</span></button>
						        	@endif
						        	
						        </p>
				          	</div>
						</div>
					@endforeach
				</div>
				<div class="x_content">
					<div class="ln_solid"></div>
		        	<form method="POST" action="{{url('/guardarArticuloImagen')}}/{{$articulo->id}}" class="form-horizontal form-label-left" accept-charset="UTF-8" enctype="multipart/form-data">
		        	<input type="hidden" name="_token" value="{{ csrf_token() }}">
		        		<div class="item form-group">
		            		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="file">Agregar imagen <span class="required">*</span>
		            		</label>
				            <div class="col-md-3 col-sm-3 col-xs-12">
		                  			<input type="file" class="form-control" name="file" required>
				            </div>
				            <div class="col-md-3 col-sm-3 col-xs-12">
		                  			<input type="submit" class="btn btn-success" value="Agregar">
				            </div>
		          		</div>
		        	</form>
	        	</div>
	      	</div>
	    </div>
    </div>
@stop