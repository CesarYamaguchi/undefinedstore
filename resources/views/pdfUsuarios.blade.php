<!DOCTYPE html>
<html lang="en" style="margin: 0;">
<head>
	<meta charset="utf-8">
	<title>Listado completo de usuarios</title>
	<link rel="stylesheet" type="text/css" href="{{asset("css/styles.css")}}">
</head>
<body>
	<div class="box" align="center">
		<h1>Lista de usuarios</h1>
	</div>
	<div align="center" class="box">
		<table class="tableMod" align="center">
			<thead>
				<tr>
					<th>#</th>
					<th>Imagen</th>
					<th>Nombre</th>
					<th>email</th>
					<th>Telefono</th>
					<th>Admin.</th>
					<th>status</th>
				</tr>
			</thead>
			@foreach($usuarios as $u)
				<tr>
					<td class="centrado">{{$u->id}}</td>
					<td class="centrado">
						<img src="{{ asset('/img/usuarios/'.$u->imagen) }}" width=50 height=50 alt="{{$u->nombre}}">
					</td>
					<td>{{$u->nombre}}</td>
					<td>{{$u->email}}</td>
					<td>{{$u->telefono}}</td>
					<td>{{$u->admin}}</td>
					<td>{{$u->status}}</td>
				</tr>
			@endforeach
		</table>
	</div>
	<footer><p align="center">Copyright &copy; JAGYM H.S. Ago-Dic 2016</p></footer>
</body>
</html>