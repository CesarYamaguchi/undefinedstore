@extends('principal')
@section('encabezado')
	<h1>Registrar Categoria</h1>
@stop

@section('contenido')
	<div class="x_panel">
	    <div class="x_title">
	        <h2>Registrar categoria</h2>
	        <div class="clearfix"></div>
	    </div>
        <div class="x_content">
			<form method="POST" action="{{url('/guardarCategoria')}}" class="form-horizontal form-label-left">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
          		<div class="item form-group">
            		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nombre">Nombre <span class="required">*</span>
            		</label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              <input id="nombre" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="nombre" placeholder="" required="required" type="text">
		            </div>
          		</div>
		        <div class="item form-group">
		            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status <span class="required">*</span>
		            </label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              <input type="checkbox" id="status" name="status" class="js-switch" checked>
		            </div>
		        </div>

          
          		<div class="ln_solid"></div>
          		<div class="form-group">
		            <div class="col-md-6 col-md-offset-3">
		           		<a href="{{url('/inicio')}}" class="btn btn-danger">Cancelar</a>
		              	<input type="submit" class="btn btn-success">
		            </div>
          		</div>
        	</form>
      	</div>
    </div>
@stop