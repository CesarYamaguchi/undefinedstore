@extends('principal')
@section('encabezado')
	<h1 align="center">{{$articulo->nombre}}</h1>
@stop
@section('contenido')
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
          	<div class="x_panel tile overflow_hidden">
	            <div class="x_title">
		            <h2>Descripción</h2>
	              	<div class="clearfix"></div>
	            </div>
	            <div class="x_content">
	            	<div class="col-xs-5" style="border-radius: 20px; border: 5px solid black; padding:10px; ">
						<img src="{{ asset('/img/articulos/'.$imagen[0]->nombre) }}" width="100%" style="border-radius: 20px">
					</div>
					<div class="col-xs-7" style="border-radius: 20px; padding: 20px;">
						<h3 style="color: black;"><strong>Descripcion del articulo:</strong></h3>
						<div width="100%">
							<h2 style="color: black;">{{$articulo->descripcion}}</h2>
						</div>
						
						<h3 style="color: black;"><strong>Precio:</strong></h3>
						<div width="100%">
							<h2 style="color: black;">$ {{$articulo->precio_venta}}</h2>
						</div>

						<h3 style="color: black;"><strong>Cantidad:</strong></h3>
						<div width="100%">
							<h2 style="color: black;">{{$articulo->cantidad}}</h2>
						</div>

						<br>
						<a href="{{url('/carrito/agregar')}}/{{Auth::user()->id}}/{{$articulo->id}}" class="btn btn-primary"><span class="fa fa-shopping-cart" aria-hidden="true"> Agregar al carrito</span></a>
					</div>
	            </div>
          	</div>
        </div>
	</div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel tile overflow_hidden">
				<div class="x_title">
					@if($cal === 0)
						<h2>Califica este producto:</h2>
					@else
						<h2>Tu calificacion para este articulo es:</h2>
					@endif
	              	<div class="clearfix"></div>
				</div>
				<div class="x_content">
					<form method="POST" action="{{url('/calificarArticulo')}}/{{Auth::user()->id}}/{{$articulo->id}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						@if($cal === 0)
							<input id="calificacion" name="calificacion" class="kv-ltr-theme-default-star rating" value="0" dir="ltr" data-size="md">

							<input type="submit" class="btn btn-success">
						@else
							<input id="calificacion" name="calificacion" class="kv-ltr-theme-default-star rating" value="{{$cal}}" dir="ltr" data-size="md" disabled>
						@endif

					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
          	<div class="x_panel tile overflow_hidden">
	            <div class="x_title">
		            <h2>Zona de comentarios</h2>
	              	<div class="clearfix"></div>
	            </div>
	            <div class="x_content">
	            	<div class="x_content">
		            	<form method="POST"  action="{{url('/guardarComentario')}}/{{ Auth::user()->id }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
							    <label  for="comentario">
							    	<h2 style="color: black;">
							    		Deja un comentario:
							    		<span class="required">*</span>
							    	</h2>
							    </label>
							    <textarea id="comentario" required="required" name="comentario" class="form-control"></textarea>
							</div>
				
							<div class="form-group">
								<input id="id_articulo" type="hidden" name="id_articulo" value={{$articulo->id}} >
							</div>

						    <input type="submit" class="btn btn-success">
						</form>
					</div>

					<div class="x_content">
						<div class="row">
							<div class="col-sm-12">
								<h3>Comentarios de este producto:</h3>
							</div><!-- /col-sm-12 -->
						</div><!-- /row -->
						@if(count($comentarios) === 0)
							<h3 style="color: black">No hay comentarios para mostrar</h3>
						@else
							@foreach ($comentarios as $c)
							<div class="row">
								<div class="col-xs-3 col-sm-2 col-md-2 col-lg-1  ">
										<img class="img-responsive user-photo" style="border-radius: 20px;" src="{{ asset('img/usuarios/'.$c->imagen) }}">
								</div><!-- /col-sm-1 -->

								<div class="col-xs-9 col-sm-10 col-md-10 col-lg-11 ">
									<div class="panel panel-default">
										<div class="panel-heading">
											<strong>{{$c->name}}</strong> 
											<span class="text-muted">
												comento el {{$c->created_at}} 
												@if	(Auth::user()->admin == 1)
													<a style="float: right;" href="{{url('/eliminarComentario')}}/{{$c->id}}" class="btn btn-danger btn-xs" alt="Eliminar"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
												@else
													<a style="float: right;" href="{{url('/reportarComentario')}}/{{$c->id}}" class="btn btn-warning btn-xs" alt="Reportar"><span class="glyphicon glyphicon-flag" aria-hidden="true"></span></a>
												@endif
											</span>
										</div>
										<div class="panel-body">
											{{$c->comentario}}
										</div><!-- /panel-body -->
									</div><!-- /panel panel-default -->
								</div><!-- /col-sm-5 -->
							</div><!-- /row -->
							@endforeach
						@endif
					</div>
	            </div>
          	</div>
        </div>
	</div>
@stop
