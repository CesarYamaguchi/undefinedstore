@extends('principal')
@section('encabezado')
	
@stop

@section('contenido')
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
	  <!-- Indicadores -->
	  <ol class="carousel-indicators">
	    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	    <li data-target="#myCarousel" data-slide-to="1"></li>
	    <li data-target="#myCarousel" data-slide-to="2"></li>
	  </ol>

	  <!-- Imagenes a mostrar -->
	  <div class="carousel-inner" role="listbox">
	    <div class="item active">
	      <img src="{{asset('/img/slider/0.png')}}" alt="1">
	      <div class="carousel-caption">
	        <!-- aqui le pueden meter texto -->
	      </div>
	    </div>

	    <div class="item">
	      <img src="{{asset('/img/slider/1.png')}}" alt="2">
	      <div class="carousel-caption">
	      	<!-- -->
	      </div>
	    </div>

	    <div class="item">
	      <img src="{{asset('/img/slider/2.png')}}" alt="3">
	      <div class="carousel-caption">
	        <!-- -->
	      </div>
	    </div>

	  <!-- controles siguiente y anterior -->
	  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	  </a>
	</div>

	<br>

	<div class="row">
		<div class="col-md-4 col-sm-4 col-xs-4">
	        <div class="x_panel tile overflow_hidden">
	    	    <div class="x_title">
		            <a href="{{url('/catalogo/0/0')}}"><h2>Categorias</h2></a>
	              	<div class="clearfix"></div>
	            </div>
	            <div class="x_content">
	            	<?php
	            		$catAux = $categorias[0]->id_cat;
	            		print "<a href=".asset('/catalogo/'.$catAux.'/0')."><h4>".$categorias[0]->nombre_cat."</h4></a>";
	            		print "<a href=".asset('/catalogo/'.$catAux.'/'.$categorias[0]->id_subcat.'')."><h6><span class='fa fa-circle-o'></span> ".$categorias[0]->nombre_subcat."</h6></a>";
	            		for ($i=1; $i < count($categorias); $i++) { 
	            			if ($catAux != $categorias[$i]->id_cat) {
	            				print "<a href=".asset('/catalogo/'.$categorias[$i]->id_cat.'/0')."><h4>".$categorias[$i]->nombre_cat."</h4></a>";
	            				$catAux = $categorias[$i]->id_cat;
	            			}
	            			print "<a href=".asset('/catalogo/'.$categorias[$i]->id_cat.'/'.$categorias[$i]->id_subcat.'')."><h6><span class='fa fa-circle-o'></span> ".$categorias[$i]->nombre_subcat."</h6></a>";
	            		}
	              	?>
	            </div>
          	</div>
        </div>

        <div class="col-md-8 col-sm-8 col-xs-8">
          	<div class="x_panel">
	            <div class="x_title">
		            <h2>Mejor Valorados</h2>
	              	<div class="clearfix"></div>
	            </div>
	            <div class="x_content">
	            	<div class="row">
						@foreach($mejores as $m)
	                      	<div class="col-xs-3">
		                        <div class="thumbnail">
		                          	<div class="image view view-first">
		                            	<a href="{{url('/descripcionArticulo')}}/{{$m[0]->id}}/{{Auth::user()->id}}">
		                            		<img style="width: 100%; display: block;" src="{{ asset('img/articulos/'.$m[0]->imagen) }}" alt="image" />
				                            <div class="mask">
				                              	<p>{{$m[0]->nombre}}</p>
				                              	<div class="tools tools-bottom">
					                                <a href="{{url('/descripcionArticulo')}}/{{$m[0]->id}}/{{Auth::user()->id}}"><i class="fa fa-link"></i></a>
					                                <a href="{{url('/carrito/agregar')}}/{{Auth::user()->id}}/{{$m[0]->id}}"><i class="fa fa-shopping-cart"></i></a>
				                              	</div>
				                            </div>
			                            </a>
		                          	</div>
		                          	<div class="caption">
		                          		<p>{{$m[0]->nombre}}</p>
							   	  		<p style="color: red">
							   	  			${{$m[0]->precio_venta}}
							   	  		</p>
		                          	</div>
		                        </div>
	                      	</div>
                      	@endforeach
                    </div>
	            </div>
          	</div>
        </div>
	</div>
	
@stop