@extends('principal')
@section('encabezado')
	<h1>Configurar categoria: {{$categoria->nombre}}</h1>
@stop

@section('contenido')
	<div class="x_panel">
	    <div class="x_title">
	        <h2>Agregar subcategoria</h2>
	        <div class="clearfix"></div>
	    </div>
        <div class="x_content">
        	<div class="container">
				<h1>Tipos</h1>
				<hr>
				<div>
					<form method="POST" action="{{url('/agregarSubcategoria')}}/{{$categoria->id}}">
					<input type="hidden" name="_token" value="{{csrf_token() }}">
						<div class="form-group">
							<label for="id_subcategoria">Subcategoria a asignar:</label>
							<select name="id_subcategoria" class="form-control">
								<option value="" selected>Selecione tipo... </option>
								@foreach($subcategoriasNOT as $sn)
									<option value="{{$sn->id}}">{{$sn->nombre}}</option>
								@endforeach
							</select>
						</div>
						<input type="submit" value="Agregar" class="btn btn-primary">
					</form>
				</div>
				<hr>
				<h2>Subcategorias asignadas a {{$categoria->nombre}}:</h2>
				<table class="table table-hover">
					<thead> 
						<tr>
							<th>#</th>
							<th>Subcategoria</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($subcategorias as $s)
							<tr>
								<td>{{$s->id}}</td>
								<td>{{$s->nombre}}</td>
								<td><a href="{{url('/quitarSubcategoria')}}/{{$categoria->id}}/{{$s->id}}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove" aria-hidden="true">Quitar</span></a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>

			
      	</div>
    </div>
@stop