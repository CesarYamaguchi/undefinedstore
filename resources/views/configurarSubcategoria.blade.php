@extends('principal')
@section('encabezado')
	<h1>Configurar subcategoria: {{$subcategoria->nombre}}</h1>
@stop

@section('contenido')
	<div class="x_panel">
	    <div class="x_title">
	        <h2>Agregar articulos</h2>
	        <div class="clearfix"></div>
	    </div>
        <div class="x_content">
        	<div class="container">
				<h1>Articulos</h1>
				<hr>
				<div>
					<form method="POST" action="{{url('/agregarArticulo')}}/{{$subcategoria->id}}">
					<input type="hidden" name="_token" value="{{csrf_token() }}">
						<div class="form-group">
							<label for="id_articulo">articulo a asignar:</label>
							<select name="id_articulo" class="form-control">
								<option value="" selected>Selecione articulo... </option>
								@foreach($articulosNOT as $an)
									<option value="{{$an->id}}">{{$an->nombre}}</option>
								@endforeach
							</select>
						</div>
						<input type="submit" value="Agregar" class="btn btn-primary">
					</form>
				</div>
				<hr>
				<h2>Articulos asignados a {{$subcategoria->nombre}}:</h2>
				<table class="table table-hover">
					<thead> 
						<tr>
							<th>#</th>
							<th>Articulo</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						@foreach($articulos as $a)
							<tr>
								<td>{{$a->id}}</td>
								<td>{{$a->nombre}}</td>
								<td><a href="{{url('/quitarArticulo')}}/{{$subcategoria->id}}/{{$a->id}}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove" aria-hidden="true">Quitar</span></a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
      	</div>
    </div>
@stop