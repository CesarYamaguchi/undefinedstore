@extends('principal')

@section('encabezado')
	<h1>Pedidos:</h1>
@stop

@section('contenido')
	<div class="container text-center">
		<div class="page-header">
			<h1><i class="fa fa-shopping-cart"></i> Lista:</h1>
		</div>
		@if(count($carrito))
		<p>
			<a href="{{url('/carrito/vaciar')}}" class="btn btn-danger">
				Vaciar carrito <i class="fa fa-trash"></i>
			</a>
		</p>
			<div class="table-responsive">
				<table class="table table-striped table-hover table-bordered">
					<thead>
					<tr>
						<th>#:</th>
						<th>Nombre:</th>
						<th>Descripción:</th>
						<th>Precio:</th>
						<th>Cantidad:</th>
						<th>Subtotal:</th>
						<th>Eliminar:</th>
					</tr>	
					</thead>
					<tbody>
						@foreach($carrito as $articulo)
							<tr>
								<td>{{$articulo->id}}</td>
								<td>{{$articulo->nombre}}</td>
								<td>{{$articulo->descripcion}}</td>
								<td>${{number_format($articulo->precio_venta,2)}}</td>
								<td>{{$articulo->cantidad}}</td>
								<td>${{number_format($articulo->precio_venta*$articulo->cantidad,2)}}</td>
								<td>
									<a href="{{url('/carrito/eliminar')}}/{{$articulo->id}}" class="btn btn-danger">
										<i class="fa fa-remove"></i>
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				<hr>
				<h3>
					<span class="label label-success">Total: ${{number_format($total,2)}}</span>
				<h3>
			</div>
		@else
			<h3>
				<span class="label label-warning">No hay artículos en el carrito.</span>
			</h3>
		@endif
		<hr>
		<a href="{{url('/catalogo/0/0/')}}" class="btn btn-primary">
			Seguir comprando
			<i class="fa fa-chevron-circle-left"></i>
		</a>

		<a href="{{url('/catalogo/0/0/')}}" class="btn btn-primary">
			Hacer pedido
			<i class="fa fa-chevron-circle-right"></i>
		</a>
	</div>
@stop