@extends('principal')

@section('encabezado')
	<h2>Consultar Usuarios
	<a href="{{url('/pdfUsuarios')}}">
		<span class="glyphicon glyphicon-file" aria-hiden="true"></span>
	</a></h2>
@stop

@section('contenido')
	{!! $usuarios->render() !!}
	<table class="table table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th>Nombre</th>
				<th>Telefono</th>
				<th>Dirección</th>
				<th>email</th>
				<th>Sexo</th>
				<th>Administrador</th>
				<th>Status</th>
				<th>Opciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach($usuarios as $u)
				<tr>
					<td>{{$u->id}}</td>
					<td>{{$u->name}}</td>
					<td>{{$u->telefono}}</td>
					<td>{{$u->direccion}}</td>
					<td>{{$u->email}}</td>
					<td>
						@if($u->sexo == 1) 
							Femenino
						@else
							Masculino
						@endif
					</td>
					<td>
						@if($u->admin == 1)
							Si
						@else
							No
						@endif
					</td>
					<td>
						@if($u->status == 1)
							Activo
						@else
							Inactivo
						@endif
					</td>
					<td align="center">
						<a href="{{url('/actualizarUsuario')}}/{{$u->id}}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit" aria-hidden="true">Editar</span></a>
					</td>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{!! $usuarios->render() !!}
@stop