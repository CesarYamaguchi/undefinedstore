@extends('principal')
@section('encabezado')
	<h1>Leer reporte de comentario</h1>
@stop

@section('contenido')
	<div class="x_panel">
	    <div class="x_title">
	        <h2>Datos del reporte:</h2>
	        <div class="clearfix"></div>
	    </div>
        <div class="x_content">
			<form method="POST" action="{{url('/guardarArticulo')}}" class="form-horizontal form-label-left">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
          		<div class="item form-group">
		            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Comentario reportado:
		            </label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		            	<label class="control-label">
		             		{{$reporte[0]->comentario}}
		            	</label>
		            </div>
		        </div>

          		<div class="item form-group">
            		<label class="control-label col-md-3 col-sm-3 col-xs-12"> Articulo Comentado:</label>
		            <div class="col-md-6 col-sm-6 col-xs-12" >
		            	<label class="control-label">
		            		{{$reporte[0]->nombre}}
		            	</label>
		            </div>
          		</div>
          		
		   		<div class="item form-group">
            		<label class="control-label col-md-3 col-sm-3 col-xs-12"> Usuario autor del comentario:</label>
		            <div class="col-md-6 col-sm-6 col-xs-12" >
		            	<label class="control-label">
		            		{{$reporte[0]->name}}
		            	</label>
		            </div>
          		</div>

          		<div class="item form-group">
            		<label class="control-label col-md-3 col-sm-3 col-xs-12"> Usuario que reporta:</label>
		            <div class="col-md-6 col-sm-6 col-xs-12" >
		            	<label class="control-label">
		            		{{$nombreR[0]->name}}
		            	</label>
		            </div>
          		</div>

          		<div class="item form-group">
		            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Motivo del reporte:</label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		            	<label class="control-label">
		             		{{$reporte[0]->descripcion}}
		            	</label>
		            </div>
		        </div>

		        <div class="ln_solid"></div>
          		<div class="form-group">
		            <div class="col-md-6 col-md-offset-3">
		              	<a href="{{url('/inicio')}}" class="btn btn-info">Volver</a>
		              	<a href="{{url('/descripcionArticulo')}}/{{$reporte[0]->id_articulo}}" class="btn btn-success">Ir al articulo</a>
		              	<a href="{{url('/eliminarComentario')}}/{{$reporte[0]->id_comentario}}" class="btn btn-danger">Eliminar Comentario</a>
		            </div>
          		</div>
        	</form>
      	</div>
    </div>
@stop