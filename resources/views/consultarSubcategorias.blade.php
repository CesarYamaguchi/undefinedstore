@extends('principal')

@section('encabezado')
	<h2>Consultar Subcategorias
	<a href="{{url('/pdfSubcategorias')}}">
		<span class="glyphicon glyphicon-file" aria-hiden="true"></span>
	</a></h2>
@stop

@section('contenido')
	{!! $subcategorias->render() !!}
	<table class="table table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th>Nombre</th>
				<th>Status</th>
				<th>Opciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach($subcategorias as $s)
				<tr>
					<td>{{$s->id}}</td>
					<td>{{$s->nombre}}</td>
					<td>
						@if($s->status==1)
							Activo
						@else
							Inactivo
						@endif
					</td>
					<td align="center">
						<a href="{{url('/actualizarSubcategoria')}}/{{$s->id}}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit" aria-hidden="true">Editar</span></a>
						<a href="{{url('/configurarSubcategoria')}}/{{$s->id}}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-cog" aria-hidden="true"> Configurar</span></a>
					</td>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{!! $subcategorias->render() !!}
@stop