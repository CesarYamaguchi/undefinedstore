<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>JAGYM - LogIn</title>

	<link rel="stylesheet" href="css/login.css">
</head>
<body>
	<section class="login">
	    <form method="POST" action="{{url('/validarLogIn')}}">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	        <!-- The Username Field -->
	        <label for="usuario">Usuario
	        <input type="text" name="usuario" id="usuario" required />
	    	</label>
	        
	        <!-- The Password Field -->
	        <label for="password">Contraseña
	        <input type="password" name="password" id="password" required />
	        </label>
	        
	        <!-- The Remember Me Checkbox -->
	        <input type="checkbox" name="remember" id="remember" />
	        <label class="check" for="remember"><span></span>Recordar</label>
	        
	        <!-- Clearn both sides -->
	        <div class="clear"></div>
	        
	        <!-- Recover Button -->
	        <input type="button" value="Registrar" />
	        
	        <!-- The Login Button -->
	        <input type="submit" value="Entrar" />
	    </form>
    </section>
</body>
</html>