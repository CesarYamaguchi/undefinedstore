<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Principal</title>

  <!-- Informacion de comentarios reportados -->
  <?php
     $reportes = DB::select('
            SELECT R.id, U.name, U.imagen, A.nombre
            FROM reportes_comentarios AS R
                INNER JOIN comentarios AS C
                ON R.id_comentario = C.id
                INNER JOIN users AS U
                ON R.id_usuario = U.id
                INNER JOIN articulos AS A
                ON C.id_articulo = A.id
            WHERE R.leido = 0');

        $noReportes = DB::select('
            SELECT  *
            FROM reportes_comentarios AS R
            WHERE R.leido = 0');

        $n = count($noReportes);
  ?>

	<!-- Bootstrap -->
    <link href="{{asset("css/bootstrap.min.css")}}" rel="stylesheet">
    <link href="{{asset("css/bootstrap.css")}}" rel="stylesheet">
    <!-- <link href="{{asset("")}}" rel="stylesheet"> -->
    <!-- Font Awesome -->
    <link href="{{asset("css/font-awesome.min.css")}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{asset("css/nprogress.css")}}" rel="stylesheet">
    <!-- iCheck -->
    <link href="{{asset("css/green.css")}}" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="{{asset("css/bootstrap-progressbar-3.3.4.min.css")}}" rel="stylesheet">
    <!-- JQVMap -->
    <link href="{{asset("css/jqvmap.min.css")}}" rel="stylesheet">
    <!-- bootstrap-daterangepicker -->
    <link href="{{asset("css/daterangepicker.css")}}" rel="stylesheet">
    <!-- Switchery -->
    <link href="{{asset("css/switchery.min.css")}}" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset("css/custom.min.css")}}" rel="stylesheet">

    <link href="{{asset("css/comments.css")}}" rel="stylesheet">

    <link href="{{asset("css/star-rating.css")}}" media="all" rel="stylesheet" type="text/css" />
     
    <!-- optionally if you need to use a theme, then include the theme file as mentioned below -->
    <link href="{{asset("themes/krajee-svg/theme.css")}}" media="all" rel="stylesheet" type="text/css" />
 

</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
	          	<div class="left_col scroll-view">
	            	<div class="navbar nav_title" style="border: 0;">
	              		<a href="{{ asset('/inicio') }}" class="site_title"><i class="fa fa-laptop"></i> <span>JAGYM H. S.</span></a>
	            	</div>

	            	<div class="clearfix"></div>

		            <!-- menu profile quick info -->
		            <div class="profile">
			            <div class="profile_pic">
			               	<img src="{{ asset('img/usuarios/'.Auth::user()->imagen) }}" alt="..." class="img-circle profile_img">
			            </div>
		              	<div class="profile_info">
		                	<span>Bienvenido,</span>
		                	<h2>{{ Auth::user()->name }}</h2>
		              	</div>
		            </div>
		            <!-- /menu profile quick info -->

	            	<br />

	            	<!-- sidebar menu -->
	            	<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	              		<div class="menu_section">
	                		<h3>General</h3>
	                		<ul class="nav side-menu">
		                  		<li><a><i class="fa fa-home"></i> Inicio<span class="fa fa-chevron-down"></span></a>
			                    	<ul class="nav child_menu">
			                      		<li><a href="{{url('/')}}">Inicio</a></li>
			                      		<li><a href="{{url('/catalogo/0/0')}}">Catalogo</a></li>
			                    	</ul>
		                  		</li>
                          @if(Auth::user()->admin == 1)
  		                  		<li><a><i class="fa fa-sitemap"></i> Administrador <span class="fa fa-chevron-down"></span></a>
  				                    <ul class="nav child_menu">
  				                        <li><a><i class=" fa fa-plus-circle"></i> Registrar<span class="fa fa-chevron-down"></span></a>
  				                          	<ul class="nav child_menu">
  					                            <li><a href="{{url('/registrarArticulo')}}">Articulo</a></li>
  			                      				<li><a href="{{url('/registrarUsuario')}}">Usuario</a></li>
  			                      				<li><a href="{{url('/registrarCategoria')}}">Categoria</a></li>
  			                      				<li><a href="{{url('/registrarSubcategoria')}}">Subcategoria</a></li>
  				                          	</ul>
  				                        </li>
  				                        <li><a><i class="fa fa-table"></i> Consultar <span class="fa fa-chevron-down"></span></a>
  				                          	<ul class="nav child_menu">
  					                            <li><a href="{{url('/consultarArticulos')}}">Articulos</a></li>
  				                      			<li><a href="{{url('/consultarUsuarios')}}">Usuarios</a></li>
  				                      			<li><a href="{{url('/consultarCategorias')}}">Categorias</a></li>
  				                      			<li><a href="{{url('/consultarSubcategorias')}}">Subcategorias</a></li>
  				                          	</ul>
  				                        </li>
  				                    </ul>
  			                  	</li>
                          @endif
	                		</ul>
	              		</div>

	            	</div>
	            	<!-- /sidebar menu -->

		            <!-- /menu footer buttons -->
		            
		            <!-- /menu footer buttons -->
	          	</div>
	        </div>


	        <!-- top navigation -->
	        <div class="top_nav">
	          	<div class="nav_menu">
		            <nav>
		             	<div class="nav toggle">
		                	<a id="menu_toggle"><i class="fa fa-bars"></i></a>
		              	</div>

		              	<ul class="nav navbar-nav navbar-right">
			                <li class="">
				                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				                    <img src="{{ asset('img/usuarios/'.Auth::user()->imagen) }}" alt="">{{ Auth::user()->name }}
				                    <span class=" fa fa-angle-down"></span>
				                </a>
			                  	<ul class="dropdown-menu dropdown-usermenu pull-right">
				                    <li><a href="{{ url('/perfil') }}"> Perfil</a></li>
				                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                             Log Out
                                             <i class="fa fa-sign-out pull-right"></i>
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
			                  	</ul>
			                </li>

                      <!--  Si es usuario normal, mostrar carrito, 
                            si es admin, mostrar mensajes de comentarios reportados-->
                      @if(Auth::user()->admin == 0)
                        <li role="presentation" class="dropdown">
                          <a href="{{ url('/carrito/mostrar')}}/{{Auth::user()->id}}">
                            <i class="glyphicon glyphicon-shopping-cart"></i>
                            <!-- <span class="badge bg-green">6</span> -->
                          </a>
                        </li>
                      @else
                        <li role="presentation" class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                          <i class="fa fa-envelope-o"></i>
                          
                          @if($n>0)
                            <span class="badge bg-green">{{$n}}</span>
                          @else
                            <span class="badge">{{$n}}</span>
                          @endif
                        </a>
                        <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                          @foreach($reportes as $r)
                          <li>
                            <a href="{{url('/leerReporte')}}/{{$r->id}}">
                              <span class="image"><img src="{{ asset('img/usuarios/'.$r->imagen) }}" alt="Profile Image" /></span>
                              <span class="message">
                                  El usuario <strong>{{$r->name}}</strong> reporto un comentario referente al articulo <strong>{{$r->nombre}}</strong>
                              </span>
                            </a>
                          </li>
                          @endforeach
                        </ul>
                      @endif
		            </nav>
	          	</div>
	        </div>
	        <!-- /top navigation -->


	         <!-- page content -->
	        <div class="right_col" role="main">
	        	@yield('encabezado')
				    <hr>
				    @yield('contenido')
	        </div>
	        <!-- /page content -->


	        <!-- footer content -->
	        <footer>
		        <div class="pull-right">
		            JAGYM - Hardware Solutions &copy; Ago-Dic 2016
		        </div>
		        <div class="clearfix"></div>
	        </footer>
	        <!-- /footer content -->



		</div>
	</div>




	<!-- <script src="{{ asset("js/") }}"></script> -->


	<!-- jQuery -->
	<script src="{{ asset("js/jquery.min.js") }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset("js/bootstrap.min.js") }}"></script>
    <!-- FastClick -->
    <script src="{{ asset("js/fastclick.js") }}"></script>
    <!-- NProgress -->
    <script src="{{ asset("js/nprogress.js") }}"></script>
    <!-- Chart.js 
    <script src="{{ asset("js/Chart.min.js") }}"></script>
    <!-- gauge.js 
    <script src="{{ asset("js/gauge.min.js") }}"></script>
    <!-- bootstrap-progressbar -->
    <script src="{{ asset("js/bootstrap-progressbar.min.js") }}"></script>
    <!-- iCheck -->
    <script src="{{ asset("js/icheck.min.js") }}"></script>
    <!-- Skycons -->
    <script src="{{ asset("js/skycons.js") }}"></script>
    <!-- Flot -->
    <script src="{{ asset("js/jquery.flot.js") }}"></script>
    <script src="{{ asset("js/jquery.flot.pie.js") }}"></script>
    <script src="{{ asset("js/jquery.flot.time.js") }}"></script>
    <script src="{{ asset("js/jquery.flot.stack.js") }}"></script>
    <script src="{{ asset("js/jquery.flot.resize.js") }}"></script>
    <!-- Flot plugins -->
    <script src="{{ asset("js/jquery.flot.orderBars.js") }}"></script></script>
    <script src="{{ asset("js/jquery.flot.spline.min.js") }}"></script>
    <script src="{{ asset("js/curvedLines.js") }}"></script>
    <!-- DateJS -->
    <script src="{{ asset("js/date.js") }}"></script>
    <!-- JQVMap -->
    <!-- <script src="{{ asset("js/jquery.vmap.js") }}"></script> 
    <script src="{{ asset("js/jquery.vmap.world.js") }}"></script> -->
    <script src="{{ asset("js/jquery.vmap.sampledata.js") }}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset("js/moment.min.js") }}"></script>
    <script src="{{ asset("js/daterangepicker.js") }}"></script>
    <!-- validator -->
    <script src="{{ asset("js/validator.js") }}"></script>
    <!-- Switchery -->
    <script src="{{ asset("js/switchery.min.js") }}"></script>
    <script src="../vendors/switchery/dist/switchery.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ asset("js/custom.min.js") }}"></script>

    <script src="{{ asset("js/star-rating.js")}}" type="text/javascript"></script>
 
    <script src="{{ asset("themes/krajee-svg/theme.js")}}"></script>
 
    <script src="{{asset("js/star-rating_locale_LANG.js")}}"></script>

    <!-- Flot -->
    <script>
      $(document).ready(function() {
        var data1 = [
          [gd(2012, 1, 1), 17],
          [gd(2012, 1, 2), 74],
          [gd(2012, 1, 3), 6],
          [gd(2012, 1, 4), 39],
          [gd(2012, 1, 5), 20],
          [gd(2012, 1, 6), 85],
          [gd(2012, 1, 7), 7]
        ];

        var data2 = [
          [gd(2012, 1, 1), 82],
          [gd(2012, 1, 2), 23],
          [gd(2012, 1, 3), 66],
          [gd(2012, 1, 4), 9],
          [gd(2012, 1, 5), 119],
          [gd(2012, 1, 6), 6],
          [gd(2012, 1, 7), 9]
        ];
        $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
          data1, data2
        ], {
          series: {
            lines: {
              show: false,
              fill: true
            },
            splines: {
              show: true,
              tension: 0.4,
              lineWidth: 1,
              fill: 0.4
            },
            points: {
              radius: 0,
              show: true
            },
            shadowSize: 2
          },
          grid: {
            verticalLines: true,
            hoverable: true,
            clickable: true,
            tickColor: "#d5d5d5",
            borderWidth: 1,
            color: '#fff'
          },
          colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
          xaxis: {
            tickColor: "rgba(51, 51, 51, 0.06)",
            mode: "time",
            tickSize: [1, "day"],
            //tickLength: 10,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10
          },
          yaxis: {
            ticks: 8,
            tickColor: "rgba(51, 51, 51, 0.06)",
          },
          tooltip: false
        });

        function gd(year, month, day) {
          return new Date(year, month - 1, day).getTime();
        }
      });
    </script>
    <!-- /Flot -->

    <!-- JQVMap
    <script>
      $(document).ready(function(){
        $('#world-map-gdp').vectorMap({
            map: 'world_en',
            backgroundColor: null,
            color: '#ffffff',
            hoverOpacity: 0.7,
            selectedColor: '#666666',
            enableZoom: true,
            showTooltip: true,
            values: sample_data,
            scaleColors: ['#E6F2F0', '#149B7E'],
            normalizeFunction: 'polynomial'
        });
      });
    </script>
    <!-- /JQVMap -->

    <!-- Skycons -->
    <script>
      $(document).ready(function() {
        var icons = new Skycons({
            "color": "#73879C"
          }),
          list = [
            "clear-day", "clear-night", "partly-cloudy-day",
            "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
            "fog"
          ],
          i;

        for (i = list.length; i--;)
          icons.set(list[i], list[i]);

        icons.play();
      });
    </script>
    <!-- /Skycons -->

    <!-- Doughnut Chart 
    <script>
      $(document).ready(function(){
        var options = {
          legend: false,
          responsive: false
        };

        new Chart(document.getElementById("canvas1"), {
          type: 'doughnut',
          tooltipFillColor: "rgba(51, 51, 51, 0.55)",
          data: {
            labels: [
              "Symbian",
              "Blackberry",
              "Other",
              "Android",
              "IOS"
            ],
            datasets: [{
              data: [15, 20, 30, 10, 30],
              backgroundColor: [
                "#BDC3C7",
                "#9B59B6",
                "#E74C3C",
                "#26B99A",
                "#3498DB"
              ],
              hoverBackgroundColor: [
                "#CFD4D8",
                "#B370CF",
                "#E95E4F",
                "#36CAAB",
                "#49A9EA"
              ]
            }]
          },
          options: options
        });
      });
    </script>
    <!-- /Doughnut Chart -->
    
    <!-- bootstrap-daterangepicker -->
    <script>
      $(document).ready(function() {

        var cb = function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        };

        var optionSet1 = {
          startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          minDate: '01/01/2012',
          maxDate: '12/31/2015',
          dateLimit: {
            days: 60
          },
          showDropdowns: true,
          showWeekNumbers: true,
          timePicker: false,
          timePickerIncrement: 1,
          timePicker12Hour: true,
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          opens: 'left',
          buttonClasses: ['btn btn-default'],
          applyClass: 'btn-small btn-primary',
          cancelClass: 'btn-small',
          format: 'MM/DD/YYYY',
          separator: ' to ',
          locale: {
            applyLabel: 'Submit',
            cancelLabel: 'Clear',
            fromLabel: 'From',
            toLabel: 'To',
            customRangeLabel: 'Custom',
            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            firstDay: 1
          }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function() {
          console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function() {
          console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
          console.log("cancel event fired");
        });
        $('#options1').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function() {
          $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function() {
          $('#reportrange').data('daterangepicker').remove();
        });
      });
    </script>
    <!-- /bootstrap-daterangepicker -->

    <!-- gauge.js
    <script>
      var opts = {
          lines: 12,
          angle: 0,
          lineWidth: 0.4,
          pointer: {
              length: 0.75,
              strokeWidth: 0.042,
              color: '#1D212A'
          },
          limitMax: 'false',
          colorStart: '#1ABC9C',
          colorStop: '#1ABC9C',
          strokeColor: '#F0F3F3',
          generateGradient: true
      };
      var target = document.getElementById('foo'),
          gauge = new Gauge(target).setOptions(opts);

      gauge.maxValue = 6000;
      gauge.animationSpeed = 32;
      gauge.set(3200);
      gauge.setTextField(document.getElementById("gauge-text"));
    </script>
    <!-- /gauge.js -->
</body>
</html>