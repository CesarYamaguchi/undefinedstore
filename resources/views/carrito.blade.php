@extends('principal')
@section('encabezado')
	<h1>Carrito</h1>
@stop

@section('contenido')
	<div class="container text-center">
		<div class="page-header">
			<h1><i class="fa fa-shopping-cart"></i> Carrito de compras:</h1>
		</div>
		@if(count($articulos))
		<p>
			<a href="{{url('/carrito/vaciar')}}/{{Auth::user()->id}}" class="btn btn-danger">
				Vaciar carrito <i class="fa fa-trash"></i>
			</a>
		</p>
			<div class="table-responsive">
				<table class="table table-striped table-hover table-bordered">
					<thead>
						<tr>
							<th>Id:</th>
							<th>Nombre:</th>
							<th>Descripción:</th>
							<th>Precio:</th>
							<th>Cantidad:</th>
							<th>Subtotal:</th>
							<th>Eliminar:</th>
						</tr>	
					</thead>
					<tbody>
						@foreach($articulos as $articulo)
							<tr>
								<td>{{$articulo->id}}</td>
								<td>{{$articulo->nombre}}</td>
								<td>{{$articulo->descripcion}}</td>
								<td>${{number_format($articulo->precio_venta,2)}}</td>
								<td>
								@foreach($cantidad as $c)
									@if($c->id_articulo==$articulo->id)
										<input 
											type="number"
											id="articulo_{{$articulo->id}}"
											min="1"
											max="{{$articulo->cantidad}}"
											value="{{$c->cantidad}}"
											required="required"
										>
										<a 
											href="#"
											onclick='
												var id=$(this).data("id");
												var href=$(this).data("href");
												var cantidad=$("#articulo_"+id).val();
												window.location.href=href+"/"+id+"/"+cantidad;
											' 
											data-href="{{url('/carrito/actualizar')}}/{{Auth::user()->id}}"
											data-id="{{$articulo->id}}" 
											class="btn btn-warning btn-update-item"
										>
										<i class="fa fa-refresh"></i>
										</a>
										@break;	
									@else
										@continue;
									@endif
								@endforeach
								</td>
								<td>${{number_format($articulo->precio_venta*$c->cantidad,2)}}</td>
								<td>
									<a href="{{url('/carrito/eliminar')}}/{{Auth::user()->id}}/{{$articulo->id}}" class="btn btn-danger">
										<i class="fa fa-remove"></i>
									</a>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				<hr>
				<h3>
					<span class="label label-success">Total: 
						${{number_format($total,2)}}
					</span>
				<h3>
			</div>
		@else
			<h3>
				<span class="label label-warning">No hay artículos en el carrito.</span>
			</h3>
		@endif
		<hr>
		<a href="{{url('/catalogo/0/0/')}}" class="btn btn-primary">
			<i class="fa fa-chevron-circle-left"></i> Seguir comprando
		</a>

		<a href="{{url('/carrito/pedido')}}/{{Auth::user()->id}}" class="btn btn-primary">
			Hacer pedido
			<i class="fa fa-chevron-circle-right"></i>
		</a>
	</div>
@stop