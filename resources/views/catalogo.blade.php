@extends('principal')
@section('encabezado')
	<h1>Catalogo de articulos</h1>
@stop

@section('contenido')
	<div class="row">
        <div class="col-md-2 col-sm-2 col-xs-12">
          	<div class="x_panel tile overflow_hidden">
	            <div class="x_title">
		            <a href="{{url('/catalogo/0/0')}}"><h2>Categorias</h2></a>
	              	<div class="clearfix"></div>
	            </div>
	            <div class="x_content">
	            	<?php
	            		$catAux = $categorias[0]->id_cat;
	            		print "<a href=".asset('/catalogo/'.$catAux.'/0')."><h4>".$categorias[0]->nombre_cat."</h4></a>";
	            		print "<a href=".asset('/catalogo/'.$catAux.'/'.$categorias[0]->id_subcat.'')."><h6><span class='fa fa-circle-o'></span> ".$categorias[0]->nombre_subcat."</h6></a>";
	            		for ($i=1; $i < count($categorias); $i++) { 
	            			if ($catAux != $categorias[$i]->id_cat) {
	            				print "<a href=".asset('/catalogo/'.$categorias[$i]->id_cat.'/0')."><h4>".$categorias[$i]->nombre_cat."</h4></a>";
	            				$catAux = $categorias[$i]->id_cat;
	            			}
	            			print "<a href=".asset('/catalogo/'.$categorias[$i]->id_cat.'/'.$categorias[$i]->id_subcat.'')."><h6><span class='fa fa-circle-o'></span> ".$categorias[$i]->nombre_subcat."</h6></a>";
	            		}
	              	?>
	            </div>
          	</div>
        </div>

        <div class="col-md-10 col-sm-10 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Articulos</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
					<div class="row">
						@foreach($articulos as $a)
	                      	<div class="col-md-55">
		                        <div class="thumbnail">
		                          	<div class="image view view-first">
		                            	<img style="width: 100%; display: block;" src="{{ asset('/img/articulos/'.$a->imagen) }}" alt="image" />
		                            	<a href="{{url('/descripcionArticulo')}}/{{$a->id}}/{{Auth::user()->id}}">
				                            <div class="mask">
				                              	<p>{{$a->nombre}}</p>
				                              	<div class="tools tools-bottom">
					                                <a href="{{url('/descripcionArticulo')}}/{{$a->id}}/{{Auth::user()->id}}"><i class="fa fa-link"></i></a>
					                                <a href="{{url('/carrito/agregar')}}/{{Auth::user()->id}}/{{$a->id}}"><i class="fa fa-shopping-cart"></i></a>
				                              	</div>
				                            </div>
			                            </a>
		                          	</div>
		                          	<div class="caption">
		                          		<p>{{$a->nombre}}</p>
							   	  		<p style="color: red">
							   	  			${{$a->precio_venta}}
							   	  		</p>
		                          	</div>
		                        </div>
	                      	</div>
                      	@endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop