@extends('principal')
@section('encabezado')
	<h1>Reportar comentario</h1>
@stop

@section('contenido')
	<div class="x_panel">
	    <div class="x_title">
	        <h2>Reportar comentario</h2>
	        <div class="clearfix"></div>
	    </div>
        <div class="x_content">
			<form method="POST" action="{{url('/reportar')}}" class="form-horizontal form-label-left">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
          		<div class="item form-group">
            		<label class="control-label col-md-3 col-sm-3 col-xs-12"> Articulo Comentado:</label>
		            <div class="col-md-6 col-sm-6 col-xs-12" >
		            	<label class="control-label">
		            		{{$comentario[0]->nombre}}
		            	</label>
		            </div>
          		</div>

          		<div class="item form-group">
            		<label class="control-label col-md-3 col-sm-3 col-xs-12">Comentario hecho por:</label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              <label class="control-label">
		            		{{$comentario[0]->name}}
		            	</label>
		            </div>
          		</div>

          		<div class="item form-group">
					<input id="id_usuario" type="hidden" name="id_usuario" value={{Auth::user()->id}} >
				</div>

				<div class="item form-group">
					<input id="id_comentario" type="hidden" name="id_comentario" value={{$comentario[0]->id}} >
				</div>

          		<div class="item form-group">
		            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="descripcion"> Motivo del reporte <span class="required">*</span>
		            </label>
		            <div class="col-md-6 col-sm-6 col-xs-12">
		              <textarea id="descripcion" required="required" name="descripcion" class="form-control col-md-7 col-xs-12"></textarea>
		            </div>
		        </div>

          
          		<div class="ln_solid"></div>
          		<div class="form-group">
		            <div class="col-md-6 col-md-offset-3">
		           		<a href="{{url('/inicio')}}" class="btn btn-danger">Cancelar</a>
		              	<input type="submit" class="btn btn-success">
		            </div>
          		</div>
        	</form>
      	</div>
    </div>
@stop