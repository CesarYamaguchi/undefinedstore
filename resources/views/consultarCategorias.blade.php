@extends('principal')

@section('encabezado')
	<h2>Consultar Categorias
	<a href="{{url('/pdfCategorias')}}">
		<span class="glyphicon glyphicon-file" aria-hiden="true"></span>
	</a></h2>
@stop

@section('contenido')
	{!! $categorias->render() !!}
	<table class="table table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th>Nombre</th>
				<th>Status</th>
				<th>Opciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach($categorias as $c)
				<tr>
					<td>{{$c->id}}</td>
					<td>{{$c->nombre}}</td>
					<td>
						@if($c->status==1)
							Activo
						@else
							Inactivo
						@endif
					</td>
					<td>
						<a href="{{url('/actualizarCategoria')}}/{{$c->id}}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit" aria-hidden="true">Editar</span></a>
						<a href="{{url('/configurarCategoria')}}/{{$c->id}}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-cog" aria-hidden="true"> Configurar</span></a>
					</td>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{!! $categorias->render() !!}
@stop