@extends('principal')
@section('encabezado')
	<h1>Datos sobre pedido:</h1>
@stop

@section('contenido')
	<div class="container text-center">
		<div class="page-header">
			<h3>Pedido: {{$id_pedido}}</h3>
		</div>
		<div class="table-responsive">
				<table class="table table-striped table-hover table-bordered">
					<thead>
						<tr>
							<th>Id:</th>
							<th>Nombre:</th>
							<th>Precio:</th>
							<th>Cantidad:</th>
							<th>Subtotal:</th>
						</tr>	
					</thead>
					<tbody>
						@foreach($carrito as $car)
							<tr>
								<td>{{$car->id_articulo}}</td>
								@foreach($descripcion_articulo as $da)
									@if($car->id_articulo==$da->id)
										<td>{{$da->nombre}}</td>
										<td>${{number_format($da->precio_venta,2)}}</td>
										<td>{{$car->cantidad}}</td>
										<td>${{number_format($da->precio_venta*$car->cantidad,2)}}</td>
										@break;
									@else
										@continue;
									@endif
								@endforeach
							</tr>
						@endforeach
					</tbody>	
				</table>
				<hr>
				<h3>
					<span class="label label-success">Total: 
						${{number_format($total,2)}}
					</span>
				<h3>
			</div>
			<hr>
			<a href="{{url('/correo/enviar/')}}" class="btn btn-primary">    	Pagar
			</a>

			<a href="{{url('/carrito/pedido')}}/{{Auth::user()->id}}" class="btn btn-primary">
				Cancelar
			</a>
	</div>
@stop