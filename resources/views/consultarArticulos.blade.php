@extends('principal')

@section('encabezado')
	<h2>Consultar Articulos
	<a href="{{url('/pdfArticulos')}}">
		<span class="glyphicon glyphicon-file" aria-hiden="true"></span>
	</a></h2>
@stop

@section('contenido')
	{!! $articulos->render() !!}
	<table class="table table-hover">
		<thead>
			<tr>
				<th>#</th>
				<th>Nombre</th>
				<th>Descripción</th>
				<th>Compra ($)</th>
				<th>Venta ($)</th>
				<th>Cantidad</th>
				<th>Status</th>
				<th >Opciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach($articulos as $a)
				<tr>
					<td>{{$a->id}}</td>
					<td>{{$a->nombre}}</td>
					<td>{{$a->descripcion}}</td>
					<td>{{$a->precio_compra}}</td>
					<td>{{$a->precio_venta}}</td>
					<td>{{$a->cantidad}}</td>
					<td>
						@if($a->status==1)
							Activo
						@else
							Inactivo
						@endif
					</td>
					<td>
						<a href="{{url('/actualizarArticulo')}}/{{$a->id}}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit" aria-hidden="true"> Editar</span></a>
						<a href="{{url('/configurarArticulo')}}/{{$a->id}}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-cog" aria-hidden="true"> Configurar</span></a>
					</td>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	{!! $articulos->render() !!}
@stop