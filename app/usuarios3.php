<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class usuarios extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'nombre', 'apellidos','telefono','direccion','email', 'usuario','password', 'admin', 'status', 'sexo', 'imagen',

    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    
}
