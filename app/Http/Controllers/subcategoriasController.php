<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use App\subcategorias;
use App\articulos_subcategorias;

class subcategoriasController extends Controller
{
    public function guardar(Request $request){
    	$nombre=$request->input('nombre');
    	$status=$request->input('status');

    	if ($status == null) {
		    $status = 0;
		} else {
		    $status = 1;
		}

    	//Guardar en BD
    	$nuevo = new subcategorias;
    	$nuevo->nombre=$nombre;
        $nuevo->status=$status;
        $nuevo->save();

        return Redirect('/consultarSubcategorias');
    }

    public function consultar(){
    	$subcategorias=DB::table('subcategorias')->paginate(10);
    	return view('consultarSubcategorias', compact('subcategorias'));
    }

    public function actualizarSubcategoria($id){
        $subcategoria = subcategorias::find($id);
        return view('actualizarSubcategoria', compact('subcategoria'));
    }

    public function actualizar($id, Request $datos){
        $status=$datos->input('status');

        if ($status == null) {
            $status = 0;
        } else {
            $status = 1;
        }

        $subcategoria = subcategorias::find($id);
        $subcategoria->nombre = $datos->input('nombre');
		$subcategoria->status = $status;
        $subcategoria->save();

        return Redirect('/consultarSubcategorias');
    }

    public function configurarSubcategoria($id){
        $subcategoria = subcategorias::find($id);

        $articulos = DB::select('
            SELECT A.id, A.nombre
            FROM articulos AS A
                INNER JOIN articulos_subcategorias AS ASub
                ON A.id = ASub.id_articulo
            WHERE A.status = 1
                AND ASub.id_subcategoria = ?', [$id]);

        $articulosNOT = DB::select('
            SELECT id, nombre
            FROM articulos
            WHERE status = 1
                AND id NOT IN(  SELECT id_articulo
                                FROM articulos_subcategorias
                                WHERE id_subcategoria = ?)', [$id]);

        return view('configurarSubcategoria', compact('subcategoria', 'articulos', 'articulosNOT'));
    }

    public function agregarArticulo(Request $request, $id_subcategoria){
        $id_articulo=$request->input('id_articulo');

        //Guardar en BD
        $nuevo = new articulos_subcategorias;
        $nuevo->id_articulo= $id_articulo;
        $nuevo->id_subcategoria=$id_subcategoria;
        $nuevo->save();

        return Redirect('/configurarSubcategoria/'.$id_subcategoria);
    }

    public function quitarArticulo($id_subcategoria, $id_articulo){
        $deleted = DB::delete('
            delete from articulos_subcategorias 
            where id_articulo = ? and id_subcategoria = ?', [$id_articulo, $id_subcategoria]);

        return Redirect('/configurarSubcategoria/'.$id_subcategoria);
    }
}
