<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use Auth;
class correoController extends Controller
{
    public function enviar()
    {
    	$data=['name'=>'JAGYM Hardware Solutions'];
    	Mail::send('correo', $data,
    		function($message)
    		{
    			$message->to(Auth::user()->email, Auth::user()->name)
    					->subject('Información sobre pedido:');
    			$message->from('jagymhs@gmail.com','JAGYM Hardware Solutions');
    		}
    	);
        return Redirect('/'); 
    }
}
