<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\usuarios;
use App\articulos;

class principalController extends Controller
{

	public function LogIn(){
    	return view('LogIn');
    }

    public function validarLogIn(Request $request){
        $usuario=$request->input('usuario');
        $password=$request->input('password');

        $id = DB::select('
            select id
            from usuarios 
            where usuario = ?
                and password = ?', [$usuario, $password]);
        
        if ($id != null) {
            $user = usuarios::find($id[0]->id);
            return view('index', compact('user'));
        } else {
            return view('LogIn');
        }
        
    }

    public function index(){
        $categorias = DB::select('
            SELECT C.id AS id_cat, C.nombre AS nombre_cat, S.id AS id_subcat, S.nombre AS nombre_subcat
            FROM categorias AS C 
                INNER JOIN categorias_subcategorias AS CS
                ON C.id = CS.id_categoria
                INNER JOIN subcategorias AS S
                ON CS.id_subcategoria = S.id
            WHERE C.status = 1
                AND S.status = 1
            ORDER BY C.nombre, S.nombre');

    	
        return view('index',compact('categorias'));
    }

    public function registrarArticulo(){
    	return view('registrarArticulo');
    }

    public function registrarUsuario(){
    	return view('registrarUsuario');
    }

    public function registrarCategoria(){
        return view('registrarCategoria');
    }

    public function registrarSubcategoria(){
        return view('registrarSubcategoria');
    }

}
