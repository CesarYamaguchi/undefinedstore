<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\pedidos;
use App\articulos_pedidos;
use App\carrito;
use DB;

class pedidosController extends Controller
{
    //Generar un pedido
	public function generar($id_usuario)
	{
		/*
		1. En la pantalla poner opción hacer pedido.<-
 		2. Guardar en la tabla pedidos id del user y en la tabla articulos_pedidos registrar articulos del carrito.
		3. El administrador y el usuario deben poder ver pedidos que se han hecho.
		*/
		
		//Tomamos del carrito todos los artículos de ese usuario
		$carrito=DB::select('
			select id_articulo, cantidad from carrito where id_usuario=? 
		',[$id_usuario]);

		$descripcion_articulo=DB::select(
			'select A.id, A.nombre, round(A.precio_venta,2) as precio_venta from articulos as A inner join carrito as C on C.id_articulo=A.id where C.id_usuario=?'
		,[$id_usuario]);

		$total=$this->total($id_usuario,$descripcion_articulo);

		if(count($carrito)==0)
		{
			/*
			dd("No hay artículos en el carrito, agregue para generar un pedido.");*/
			echo "<script>
					alert('Agregue artículos al carrito, para generar reporte');
					window.history.go(-1)
				</script>";
		}
		else
		{
			//Guardar en la tabla pedidos id del user
			$pedido=new pedidos();
			$pedido->id_usuario=$id_usuario;
			$pedido->status=0;
			$pedido->save();
			//Guardar en la tabla articulos_pedidos los articulos del carrito

			$articulos_pedidos=new articulos_pedidos();
			foreach ($carrito as $articulo) 
			{
				$id_pedido=$pedido->id;
				$articulos_pedidos->id_pedido=$id_pedido;
				$articulos_pedidos->id_articulo=$articulo->id_articulo;
				$articulos_pedidos->cantidad=$articulo->cantidad;
				$articulos_pedidos->status=0;	
			}
			$articulos_pedidos->save();
			return view('pedidoGenerado',compact('id_pedido','total','carrito','descripcion_articulo'));
		}
	}

	public function cancelar($id_usuario, $id_pedido)
	{
		/*DB::delete('
        	delete from pedido
        	and id_articulo=? 
        ',[$id_usuario,$id_articulo]);
        return Redirect('/carrito/mostrar/'.$id_usuario);*/
	}

	//Total
	private function total($id_usuario,$articulos)
	{
		$total=0;
		foreach($articulos as $a)
		{
			$carrito=DB::select('
				select cantidad from carrito where id_usuario=? and id_articulo=?
			',[$id_usuario,$a->id]);
			foreach ($carrito as $c) 
			{
				$total+=$a->precio_venta * $c->cantidad;	
			}
		}
		return $total;
	}

}
