<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\comentarios;
use App\reportes_comentarios;
use App\articulos;

class comentariosController extends Controller
{
    public function guardarComentario(Request $request, $id_usuario){
    	$comentario=$request->input('comentario');
    	$id_articulo=$request->input('id_articulo');
        
    	//Guardar en BD
    	$nuevo = new comentarios;
    	$nuevo->comentario=$comentario;
        $nuevo->id_articulo=$id_articulo;
        $nuevo->id_usuario=$id_usuario;
        $nuevo->status=0;
        $nuevo->save();

        return redirect()->back();
    }

    public function reportarComentario($id_comentario){
        $comentario = DB::select('
            SELECT C.id, C.id_usuario, U.name, A.nombre
            FROM comentarios AS C
                INNER JOIN users AS U
                ON C.id_usuario = U.id
                INNER JOIN articulos AS A
                ON C.id_articulo  = A.id
            WHERE C.id = ?',[$id_comentario]);

        return view('reportarComentario', compact('comentario'));
    }

    public function reportar(Request $request){
        $id_usuario = $request->input('id_usuario');
        $id_comentario = $request->input('id_comentario');
        $descripcion = $request->input('descripcion');

        $nuevo = new reportes_comentarios;
        $nuevo->id_usuario=$id_usuario;
        $nuevo->id_comentario=$id_comentario;
        $nuevo->descripcion=$descripcion;
        $nuevo->leido=0;
        $nuevo->status=0;
        $nuevo->save();

        $comentario = comentarios::find($id_comentario);
        $articulos = articulos::find($comentario->id_articulo);
        $id=$articulos->id;

        return redirect()->action('articulosController@descripcionArticulo', ['id' => $id, 'id_usuario' =>$id_usuario]);

    }

    public function leerReporte($id_reporte){
        
        $reporte = reportes_comentarios::find($id_reporte);
        $reporte->leido=1;
        $reporte->save();

        $reporte = DB::select('
            SELECT R.id_comentario,R.id_usuario, R.descripcion, U.name, A.nombre,C.comentario,C.id_articulo 
            FROM reportes_comentarios AS R
                INNER JOIN comentarios as C
                ON R.id_comentario = C.id
                INNER JOIN articulos AS A
                ON C.id_articulo = A.id
                INNER JOIN users AS U
                ON C.id_usuario = U.id
            WHERE R.id = ?',[$id_reporte]);

        
        $nombreR= DB::select('
            SELECT name 
            FROM users 
            WHERE id = ?',[$reporte[0]->id_usuario]);

        return view('leerReporte', compact('reporte','nombreR','id_reporte'));
    }

    public function eliminarComentario($id){
        $comentario = comentarios::find($id); 
        $id= $comentario->id_articulo;
        $comentario->status=1;
        $id_usuario = $comentario->id_usuario;
        $comentario->save();

        return redirect()->action('articulosController@descripcionArticulo', ['id' => $id, 'id_usuario'=>$id_usuario]);
    }
}
