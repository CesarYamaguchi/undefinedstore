<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use PDF;
use App\User;
use DB;

class usuariosController extends Controller
{
    public function guardar(Request $request){
    	$name=$request->input('nombre');
        $email=$request->input('email');
        $password=$request->input('password');
        $sexo=$request->input('sexo');
    	$telefono=$request->input('telefono');
    	$direccion=$request->input('direccion');
    	$admin=$request->input('admin');
    	$imagen=$request->file('file');
    	$status=$request->input('status');
        
        if($imagen == null){
            $nombreImg = "";
        } else{
            $nombreImg = $imagen->getClientOriginalName();
        }
        
        //dd($nombreImg);
    	if ($sexo == 0) {
		    $sexo = 0;
		} else {
		    $sexo = 1;
		}
    	if ($admin == null) {
		    $admin = 0;
		} else {
		    $admin = 1;
		}
    	if ($status == null) {
		    $status = 0;
		} else {
		    $status = 1;
		}


    	//Guardar en BD
    	$nuevo = new User;
        $nuevo->name=$name;
        $nuevo->email=$email;
        $nuevo->password=$password;
    	$nuevo->sexo=$sexo;
        $nuevo->telefono=$telefono;
        $nuevo->direccion=$direccion;
        $nuevo->admin=$admin;
        $nuevo->imagen=$nombreImg;
        $nuevo->status=$status;
        $nuevo->save();

        if($imagen != null){
            \Storage::disk('local-usuarios')->put($nombreImg,  \File::get($imagen));
        }
        

        return Redirect('/consultarUsuarios');
    }

    public function consultar(){
    	$usuarios=DB::table('users')->paginate(10);
    	return view('consultarUsuarios', compact('usuarios'));
    }

    public function actualizarUsuario($id){
        $usuario = User::find($id);
        return view('actualizarUsuario', compact('usuario'));
    }

    public function actualizar($id, Request $datos){
    	$sexo=$datos->input('sexo');
        $admin=$datos->input('admin');
        $status=$datos->input('status');
        $imagen=$datos->file('file');
        
        if($imagen == null){
            $nombreImg = "";
        } else{
            $nombreImg = $imagen->getClientOriginalName();
        }

        if ($sexo == 0) {
		    $sexo = 0;
		} else {
		    $sexo = 1;
		}
        if ($admin == null) {
		    $admin = 0;
		} else {
		    $admin = 1;
		}
    	if ($status == null) {
		    $status = 0;
		} else {
		    $status = 1;
		}

        $usuario = User::find($id);
        $usuario->name = $datos->input('nombre');
        $usuario->email = $datos->input('email');
        $usuario->sexo = $sexo;
        $usuario->telefono = $datos->input('telefono');
        $usuario->direccion = $datos->input('direccion');
        $usuario->imagen = $nombreImg;
        $usuario->admin = $admin;
		$usuario->status = $status;
        $usuario->save();

        if($imagen != null){
            \Storage::disk('local-usuarios')->put($nombreImg,  \File::get($imagen));
        }

        return Redirect('/consultarUsuarios');
    }

    public function perfil(){
        return view('perfil');
    }

    public function pdfUsuarios(){
        $usuarios=User::all();

        $vista=view('pdfUsuarios', compact('usuarios'));
        $dompdf=\App::make('dompdf.wrapper');
        $dompdf->loadHTML($vista);
        return $dompdf->stream();
    }
}
