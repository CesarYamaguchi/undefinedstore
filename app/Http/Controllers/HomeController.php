<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\articulos;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = DB::select('
            SELECT C.id AS id_cat, C.nombre AS nombre_cat, S.id AS id_subcat, S.nombre AS nombre_subcat
            FROM categorias AS C 
                INNER JOIN categorias_subcategorias AS CS
                ON C.id = CS.id_categoria
                INNER JOIN subcategorias AS S
                ON CS.id_subcategoria = S.id
            WHERE C.status = 1
                AND S.status = 1
            ORDER BY C.nombre, S.nombre');

        $articulos=articulos::all();

        for ($i = 0 ; $i < count($articulos); $i++) {
            $conteo[$i] =  DB::select('SELECT SUM(C.calificacion) AS cali FROM calificaciones AS C WHERE C.id_articulo = ? GROUP BY C.id_articulo', [$articulos[$i]->id]);
        }
         
       for ($i = count($articulos)-1 ; $i >= 0; $i--) {
           
           $aux=array_pop($conteo);
          
           if ($aux == null) {
                $cali=0;
           }else{
                $cali=$aux[0]->cali;
           }

           $a = (int) $cali;

           $articulo_cali[$i] = ['id_articulo'=>$articulos[$i]->id,'calif' => $a];
       }
 
       foreach ($articulo_cali as $a => $fila) {
            $id_articulo[$a] = $fila['id_articulo'];
            $calif[$a] = $fila['calif'];
        }
       
       array_multisort($calif, SORT_DESC, $id_articulo, SORT_ASC, $articulo_cali);

        $mejores = array();  
        for ($i=0; $i < 4 ; $i++) {
          array_push($mejores, DB::select('
                SELECT A.nombre, A.precio_venta, A.id, I.nombre AS imagen FROM articulos as A
                    INNER JOIN articulo_imagenes AS I
                    ON A.id = I.id_articulo 
                WHERE I.principal=1 
                AND A.id = ?', [$articulo_cali[$i]['id_articulo']]
                ));  
        }

        return view('index',compact('categorias','mejores'));
    }
}
