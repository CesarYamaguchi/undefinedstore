<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\articulos;
use App\carrito;
use DB;

class carritoController extends Controller
{

    //Mostrar carrito
	public function mostrar($id_usuario)
	{
		/*$articulos=DB::table('articulos as A')
			->select('A.id','A.nombre','A.descripcion','A.precio_venta','A.cantidad')
			->join('carrito as C','C.id_articulo','=','A.id')
			->where('C.id_usuario','=',$id_usuario)
			->get();*/
		$articulos=DB::select(
			'select A.id, A.nombre, A.descripcion, round(A.precio_venta,2) as precio_venta, A.cantidad from articulos as A inner join carrito as C on C.id_articulo=A.id where C.id_usuario=?'
		,[$id_usuario]);
		$cantidad=DB::table('carrito')
			->select('id_articulo','cantidad')
			->where('id_usuario','=',$id_usuario)
			->get();
		$total=$this->total($id_usuario, $articulos);
		return view('carrito', compact('articulos','total','cantidad'));
	}

    //Añadir artículo
	public function agregar($id_usuario,$id_articulo)
	{
		$esta=DB::select('
			select cantidad from carrito where id_usuario=?
			and id_articulo=? 
		',[$id_usuario,$id_articulo]);

		if(count($esta)>0)
		{
			//Luego le sumamos uno a la cantidad
			foreach ($esta as $cantidad)
			{
				$aux=$cantidad->cantidad;
				$aux+=1;
			}

			//Actualizamos el carrito para que se puedan agregar más articulos del mismo tipo
			DB::update('
				update carrito set cantidad=? where id_usuario=?
				and id_articulo=?
			',[$aux,$id_usuario,$id_articulo]);			
		}
		else
		{
			$carrito=new carrito();
			$carrito->id_usuario=$id_usuario;
			$carrito->id_articulo=$id_articulo;
			$carrito->cantidad='1';
			$carrito->save();
		}
		return Redirect('/carrito/mostrar/'.$id_usuario);
	}

    //Borrar artículo
	public function eliminar($id_usuario, $id_articulo)
	{
        DB::delete('
        	delete from carrito where id_usuario=?
        	and id_articulo=? 
        ',[$id_usuario,$id_articulo]);
        return Redirect('/carrito/mostrar/'.$id_usuario);
	}

	//Vaciar carrito
	public function vaciar($id_usuario)
	{
		DB::delete('
			delete from carrito where id_usuario=?
		',[$id_usuario]);
		return Redirect('/carrito/mostrar/'.$id_usuario);
	}

    //Total
	private function total($id_usuario,$articulos)
	{
		$total=0;
		foreach($articulos as $a)
		{
			$carrito=DB::select('
				select cantidad from carrito where id_usuario=? and id_articulo=?
			',[$id_usuario,$a->id]);
			foreach ($carrito as $c) 
			{
				$total+=$a->precio_venta * $c->cantidad;	
			}
		}
		return $total;
	}

	//Actualizar la cantidad de articulos
	public function actualizar($id_usuario, $id_articulo, $cantidad)
	{
		DB::update('
				update carrito set cantidad=? where id_usuario=?
				and id_articulo=?
			',[$cantidad,$id_usuario,$id_articulo]);
		return Redirect('/carrito/mostrar/'.$id_usuario);
	}
	
}
