<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use App\articulo_imagenes;

class articulo_imagenesController extends Controller
{
    public function guardar(Request $request, $id_articulo){
		
    	$imagen=$request->file('file');

    	$nombre=$imagen->getClientOriginalName();

    	$existe = \Storage::disk('local-articulos')->exists($nombre);
    	
    	//Guardar en BD
    	$nuevo = new articulo_imagenes;
    	$nuevo->nombre=$nombre;
        $nuevo->principal=0;
        $nuevo->id_articulo=$id_articulo;
        $nuevo->save();

        //guardar imagen en servidor solo si no existe
     	if (!$existe){
     		\Storage::disk('local-articulos')->put($nombre,  \File::get($imagen));
     	}
        
        return Redirect('/configurarArticulo/'.$id_articulo);
    }

    public function quitarImg($id_articulo, $id_img){
    	articulo_imagenes::find($id_img)->delete();
    	return Redirect('/configurarArticulo/'.$id_articulo);
    }

    public function principalImg($id_articulo, $id_img){
    	DB::beginTransaction();
		    DB::update('
		    	UPDATE articulo_imagenes SET 
		    	principal = 0 
		    	WHERE id_articulo = ?', [$id_articulo]);

		    DB::update('
		    	UPDATE articulo_imagenes SET 
		    	principal = 1 
		    	WHERE id = ?', [$id_img]);
		DB::commit();

		return Redirect('/configurarArticulo/'.$id_articulo);
    }
}
