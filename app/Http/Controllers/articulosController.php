<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use App\articulos;
use App\articulo_imagenes;
use App\usuarios;
use App\calificaciones;

class articulosController extends Controller
{
    public function guardar(Request $request){
    	$nombre=$request->input('nombre');
    	$descripcion=$request->input('descripcion');
    	$precio_compra=$request->input('precio_compra');
    	$precio_venta=$request->input('precio_venta');
    	$cantidad=$request->input('cantidad');
    	$status=$request->input('status');

    	if ($status == null) {
		    $status = 0;
		} else {
		    $status = 1;
		}

    	//Guardar en BD
    	$nuevo = new articulos;
    	$nuevo->nombre=$nombre;
        $nuevo->descripcion=$descripcion;
        $nuevo->precio_compra=$precio_compra;
        $nuevo->precio_venta=$precio_venta;
        $nuevo->cantidad=$cantidad;
        $nuevo->status=$status;
        $nuevo->save();

        return Redirect('/consultarArticulos');
    }

    public function consultar(){
    	$articulos=DB::table('articulos')->paginate(10);
    	return view('consultarArticulos', compact('articulos'));
    }

    public function actualizarArticulo($id){
        $articulo = articulos::find($id);
        return view('actualizarArticulo', compact('articulo'));
    }

    public function actualizar($id, Request $datos){
        $status=$datos->input('status');

        if ($status == null) {
            $status = 0;
        } else {
            $status = 1;
        }

        $articulo = articulos::find($id);
        $articulo->nombre = $datos->input('nombre');
        $articulo->descripcion = $datos->input('descripcion');
        $articulo->precio_compra = $datos->input('precio_compra');
        $articulo->precio_venta = $datos->input('precio_venta');
        $articulo->cantidad = $datos->input('cantidad');
		$articulo->status = $status;
        $articulo->save();

        return Redirect('/consultarArticulos');
    }

    public function configurarArticulo($id){
        $articulo = articulos::find($id);
        $imagenes = DB::select('
            SELECT id, nombre, principal
            FROM articulo_imagenes
            WHERE id_articulo = ?', [$id]);

        return view('configurarArticulo', compact('articulo', 'imagenes'));
    }

    public function catalogo($categoria, $subcategoria){
        $cond_categoria = "";
        $cond_subcategoria = "";
        if ($categoria != 0) {
            $cond_categoria = " AND CS.id_categoria = ".$categoria." ";
        }
        if ($subcategoria != 0) {
            $cond_subcategoria = " AND ASub.id_subcategoria = ".$subcategoria." ";
        }

        $articulos = DB::select('
            SELECT DISTINCT A.id, A.nombre, AI.nombre AS imagen, ROUND(A.precio_venta, 2) AS precio_venta
            FROM articulos AS A
                LEFT JOIN articulos_subcategorias AS ASub
                ON A.id = ASub.id_articulo
                LEFT JOIN categorias_subcategorias AS CS
                ON ASub.id_subcategoria = CS.id_subcategoria
                LEFT JOIN articulo_imagenes AS AI
                ON A.id = AI.id_articulo
            WHERE AI.principal = 1
                AND A.status = 1
                '.$cond_categoria.$cond_subcategoria.'
                OR AI.id IS NULL');

        $categorias = DB::select('
            SELECT C.id AS id_cat, C.nombre AS nombre_cat, S.id AS id_subcat, S.nombre AS nombre_subcat
            FROM categorias AS C 
                INNER JOIN categorias_subcategorias AS CS
                ON C.id = CS.id_categoria
                INNER JOIN subcategorias AS S
                ON CS.id_subcategoria = S.id
            WHERE C.status = 1
                AND S.status = 1
            ORDER BY C.nombre, S.nombre');
        //dd($articulos);
        return view('catalogo', compact('articulos', 'categorias'));
    }

    public function descripcionArticulo($id_articulo, $id_usuario){
        $articulo = articulos::find($id_articulo);
        
        $imagen = DB::select('
            SELECT nombre
            FROM articulo_imagenes
            WHERE id_articulo = ? AND principal = 1', [$id_articulo]);

        $comentarios = DB::select('
        	SELECT C.id, C.comentario, C.created_at, U.name, U.imagen
			FROM comentarios AS C
				INNER JOIN users AS U
				ON C.id_usuario = U.id
        	WHERE C.id_articulo = ? AND C.status = 0
        	ORDER BY C.created_at DESC',[$id_articulo]);

        $calificacion = DB::select('
                SELECT C.calificacion
                FROM calificaciones as C
                WHERE C.id_articulo = ? AND C.id_usuario = ?', [$id_articulo, $id_usuario]);

        $conteo = count($calificacion);

        if($conteo != 0){
            $cal = $calificacion[0]->calificacion;
        }else{
            $cal = 0;
        }

        return view('descripcionArticulo', compact('articulo','imagen','comentarios','cal'));
    }

    public function pdfArticulos(){
        $articulos=articulos::all();
        $vista=view('pdfArticulos', compact('articulos'));
        $dompdf=\App::make('dompdf.wrapper');
        $dompdf->loadHTML($vista);
        return $dompdf->stream();
    }

    public function calificarArticulo($id_usuario, $id_articulo, Request $cal){
        $calificacion=$cal->input('calificacion');
        
        $nuevo = new calificaciones;
        $nuevo->calificacion=$calificacion;
        $nuevo->id_articulo=$id_articulo;
        $nuevo->id_usuario=$id_usuario;
        $nuevo->status=0;
        $nuevo->save();

        return redirect()->back();
    }
}
