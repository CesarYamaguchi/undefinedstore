<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;
use App\categorias;
use App\categorias_subcategorias;

class categoriasController extends Controller
{
    public function guardar(Request $request){
    	$nombre=$request->input('nombre');
    	$status=$request->input('status');

    	if ($status == null) {
		    $status = 0;
		} else {
		    $status = 1;
		}

    	//Guardar en BD
    	$nuevo = new categorias;
    	$nuevo->nombre=$nombre;
        $nuevo->status=$status;
        $nuevo->save();

        return Redirect('/consultarCategorias');
    }

    public function consultar(){
    	$categorias=DB::table('categorias')->paginate(10);
    	return view('consultarCategorias', compact('categorias'));
    }

    public function actualizarCategoria($id){
        $categoria = categorias::find($id);
        return view('actualizarCategoria', compact('categoria'));
    }

    public function actualizar($id, Request $datos){
        $status=$datos->input('status');

        if ($status == null) {
            $status = 0;
        } else {
            $status = 1;
        }

        $categoria = categorias::find($id);
        $categoria->nombre = $datos->input('nombre');
		$categoria->status = $status;
        $categoria->save();

        return Redirect('/consultarCategorias');
    }

    public function configurarCategoria($id){
        $categoria = categorias::find($id);

        $subcategorias = DB::select('
            SELECT S.id, S.nombre
            FROM subcategorias AS S
                INNER JOIN categorias_subcategorias AS CS
                ON S.id = CS.id_subcategoria
            WHERE S.status = 1
                AND CS.id_categoria = ?', [$id]);

        $subcategoriasNOT = DB::select('
            SELECT id, nombre
            FROM subcategorias
            WHERE status = 1
                AND id NOT IN(  SELECT id_subcategoria
                                FROM categorias_subcategorias
                                WHERE id_categoria = ?)', [$id]);

        return view('configurarCategoria', compact('categoria', 'subcategorias', 'subcategoriasNOT'));
    }

    public function agregarSubcategoria(Request $request, $id_categoria){
        $id_subcategoria=$request->input('id_subcategoria');

        //Guardar en BD
        $nuevo = new categorias_subcategorias;
        $nuevo->id_categoria= $id_categoria;
        $nuevo->id_subcategoria=$id_subcategoria;
        $nuevo->save();

        return Redirect('/configurarCategoria/'.$id_categoria);
    }

    public function quitarSubcategoria($id_categoria, $id_subcategoria){
        $deleted = DB::delete('
            delete from categorias_subcategorias 
            where id_categoria = ? and id_subcategoria = ?', [$id_categoria, $id_subcategoria]);

        return Redirect('/configurarCategoria/'.$id_categoria);
    }

     public function pdfCategorias(){
        $categorias=categorias::all();
        $vista=view('pdfCategorias', compact('categorias'));
        $dompdf=\App::make('dompdf.wrapper');
        $dompdf->loadHTML($vista);
        return $dompdf->stream();
    }
}
