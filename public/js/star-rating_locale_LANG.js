/*!
 * Star Rating <LANG> Translations
 *
 * This file must be loaded after 'star-rating.js'. Patterns in braces '{}', or
 * any HTML markup tags in the messages must not be converted or translated.
 *
 * @see http://github.com/kartik-v/bootstrap-star-rating
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
(function ($) {
    "use strict";
    $.fn.ratingLocales['<LANG>'] = {
        defaultCaption: '{rating} Stars',
        starCaptions: {
            1: 'Una Estrella',
            2: 'Dos Estrellas',
            3: 'Tres Estrellas',
            4: 'Cuatro Estrellas',
            5: 'Cinco Estrellas'
        },
        clearButtonTitle: 'Limpiar',
        clearCaption: 'No Valorado'
    };
})(window.jQuery);
